## DotNetty 

### Netty 介绍

```
Netty 是一款用于创建高性能网络应用程序的高级框架。

Netty 是一款异步的事件驱动的网络应用程序框架，支持快速地开发可维护的高性能的面向协议的服务器和客户端
```

### DotNetty 介绍

``` 
DotNetty是微软的Azure团队仿造Netty编写的网络应用程序框架。
```

### 优点

- 关注点分离——业务和网络逻辑解耦
- 模块化和可复用性
- 可测试性作为首要的要求

### 阻塞Socket特点

- 建立连接要阻塞线程，读取数据要阻塞线程
- 如果要管理多个客户端，就需要为每个客户端建立不同的线程
- 会有大量的线程在休眠状态，等待接收数据，资源浪费
- 每个线程都要占用系统资源
- 线程的切换很耗费系统资源

### 非阻塞Socket特点

- 每个Socket如果需要读写操作，都通过事件通知的方式通知选择器，这样就实现了一个线程管理多个Socket的目的
- 选择器甚至可以在所有的Socket空闲的时候允许线程先去干别的事情
- 减少了线程数量导致的资源占用，减少了线程切换导致的资源消耗

### 核心组件

- Channel：一个连接就是一个Channel，Socket的封装，提供绑定，读，写等操作，降低了直接使用Socket的复杂性

- 回调：通知的基础

  ```c#
  public class ConnectHandler : SimpleChannelInboundHandler<string>
  {
      public override void ChannelActive(IChannelHandlerContext context)
      {
          // 新的连接建立的时候会触发这个回调
          base.ChannelActive(context);
      }
      protected override void ChannelRead0(IChannelHandlerContext ctx, string msg)
      {
          throw new NotImplementedException();
      }
  }
  ```

- Future（ChannelFuture）：通知的另一种方式，可以认为ChannelFuture是包装了一系列Channel事件的对象。回调和Future相互补充，相互结合同时也可以理解Future是一种更加精细的回调（ChannelFuture在DotNetty中被Task取代）

- ChannelHandler：处理数据的逻辑容器，接收并处理入站事件的逻辑容器，可以处理入站数据以及给客户端以回复。通常每一个事件都由一系列的Handler处理

- ChannelPipeline：将ChannelHandler穿成一串的的容器

  - ChannelInboundHandler只处理入站事件
  - ChannelOutboundHandler只处理出站事件
  - ChannelInboundHandler和ChannelOutboundHandler可以注册在同一个ChannelPipeline中

- EventLoop：

  - 一个 EventLoopGroup 包含一个或者多个 EventLoop

  - 一个 EventLoop 在它的生命周期内只和一个 Thread 绑定

  - 所有由 EventLoop 处理的 I/O 事件都将在它专有的 Thread 上被处理

  - 一个 Channel 在它的生命周期内只注册于一个 EventLoop

  - 一个 EventLoop 可能会被分配给一个或多个 Channel

  - 心跳检测：

    ```c#
    ctx.Channel.EventLoop.Schedule(() =>
    {
        Console.WriteLine("delay 1s");
    }, new TimeSpan(1000));
    // 如果需要提前取消，可以调用Cancel方法
    IScheduledTask task = ctx.Channel.EventLoop.Schedule(() =>
    {
        Console.WriteLine("delay 1s");
    }, new TimeSpan(1000));
    tsak.Cancel();
    ```

    

- Bootstrap（引导类）：

  - Bootstrap用于引导客户端，ServerBootstrap用于引导服务器
  - 客户端引导类只需要一个EventLoopGroup，服务器引导类需要两个EventLoopGroup（服务器的第一个EventLoopGroup只有一个EventLoop，只含有一个SeverChannel用于监听本地端口，一旦连接建立，这个EventLoop就将Channel控制权移交给另一个EventLoopGroup，这个EventLoopGroup分配一个EventLoop给Channel用于管理这个Channel）

- ByteBuffer：AbstractByteBuffer，IByteBuffer，IByteBufferHolder

  - 优点：

    - 可以被自定义的缓冲区类型扩展
    - 通过内置的复合缓冲区类型实现了透明的零拷贝
    - 容量可以按需增长（类似于 JDK 的 StringBuilder）
    - 在读和写这两种模式之间切换不需要调用 ByteBuffer 的 flip()方法
    - 读和写使用了不同的索引
    - 支持方法的链式调用
    - 支持引用计数
    - 支持池化

  - 原理：每一个 IByteBuffer 都有两个索引，读索引和写索引，read和write会移动索引，set和get不会引动索引

  - 使用：

    - 堆缓冲区：使用支撑数组给 IByteBuffer 提供快速的分配和释放的能力。适用于有遗留数据需要处理的情况

      ```c#
      public override void ChannelRead(IChannelHandlerContext ctx, object msg)
      {
          IByteBuffer message = msg as IByteBuffer;
          // 检查是否有支撑数组
          if (message.HasArray)
          {
              // 获取数组
              byte[] array = message.Array;
              // 计算第一个字节的偏移
              int offset = message.ArrayOffset + message.ReaderIndex;
              // 获取可读字节数
              int length = message.ReadableBytes;
              // 调用方法，处理数据
              HandleArray(array, offset, length);
          }
          Console.WriteLine("收到信息：" + message.ToString(Encoding.UTF8));
          ctx.WriteAsync(message);
      }
      ```

    - 直接缓冲区

      ```c#
      public override void ChannelRead(IChannelHandlerContext ctx, object msg)
      {
          IByteBuffer message = msg as IByteBuffer;
          if (message.HasArray)
          {
              int length = message.ReadableBytes;
              byte[] array = new byte[length];
              message.GetBytes(message.ReaderIndex, array);
              HandleArray(array, 0, length);
          }
          Console.WriteLine("收到信息：" + message.ToString(Encoding.UTF8));
          ctx.WriteAsync(message);
      }
      ```

    - CompositeByteBuffer 复合缓冲区：如果要发送的命令是由两个 IByteBuffer 拼接构成的，那么就需要复合缓冲区，比如Http协议中一个数据流由头跟内容构成这样的逻辑

      ```c#
      public override void ChannelRead(IChannelHandlerContext ctx, object msg)
      {
          IByteBuffer message = msg as IByteBuffer;
          // 创建一个复合缓冲区
          CompositeByteBuffer messageBuf = Unpooled.CompositeBuffer();
          // 创建两个ByteBuffer
          IByteBuffer headBuf = Unpooled.CopiedBuffer(message);
          IByteBuffer bodyBuf = Unpooled.CopiedBuffer(message);
          // 添加到符合缓冲区中
          messageBuf.AddComponents(headBuf, bodyBuf);
          // 删除
          messageBuf.RemoveComponent(0);
      
          Console.WriteLine("收到信息：" + message.ToString(Encoding.UTF8));
          ctx.WriteAsync(message);
      }
      ```

  - 字节级操作

    - 读取(不移动索引)

      ```c#
      public override void ChannelRead(IChannelHandlerContext ctx, object msg)
      {
          IByteBuffer message = msg as IByteBuffer;
          for (int i = 0; i < message.Capacity; i++)
          {
              // 如此使用索引访问不会改变读索引也不会改变写索引
              byte b = message.GetByte(i);
              Console.WriteLine(b);
          }
          Console.WriteLine("收到信息：" + message.ToString(Encoding.UTF8));
          ctx.WriteAsync(message);
      }
      ```

    - 丢弃可丢弃字节：调用read方法之后，readindex已经移动过了的区域，这段区域的字节称为可丢弃字节

      ```c#
      message.DiscardReadBytes();
      ```

    - 读取所有可读字节（移动读索引）

      ```c#
      while (message.IsReadable())
      {
          Console.WriteLine(message.ReadByte());
      }
      ```

    - 写入数据

      ```c#
      // 使用随机数填充可写区域
      while (message.WritableBytes > 4)
      {
          message.WriteInt(new Random().Next(0, 100));
      }
      ```

    - 管理索引

      - MarkReaderIndex，ResetReaderIndex 标记和恢复读索引
      - MarkWriterIndex，ResetWriterIndex 标记和恢复写索引
      - SetReaderIndex(int)，SetWriterIndex(int) 直接移动索引
      - clear() 重置两个索引都为0，但是不会清除内容

    - 查找

      - IndexOf()
      - 使用Processor

    - 派生：本质上指向同一个对象

    - 复制：复制出一个独立的对象

    - 释放

      ```c#
      // 显式丢弃消息
      ReferenceCountUtil.release(msg);
      ```

    - 增加引用计数防止释放

      ```c#
      ReferenceCountUtil.retain(message)
      ```

      

### 包介绍

- DotNetty.Common：公共的类库项目，包装线程池，并行任务和常用帮助类的封装
- DotNetty.Transport：是DotNetty核心的实现
- DotNetty.Buffers：是对内存缓冲区管理的封装
- DotNetty.Codes：是对编码器解码器的封装，包括一些基础基类的实现
- DotNetty.Handlers：封装了常用的管道处理器，比如Tls编解码，超时机制，心跳检查，日志等
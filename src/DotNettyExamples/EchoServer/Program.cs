﻿using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Sockets;
using System;
using System.Net;
using System.Threading.Tasks;

namespace EchoServer
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            await RunServerAsync();

        }

        static async Task RunServerAsync()
        {
            IEventLoopGroup eventLoop = new MultithreadEventLoopGroup();
            try
            {
                //启动一个网络应用程序
                var bootstrap = new ServerBootstrap();
                //EventLoopGroup 是一系列EventLoop的集合
                //EventLoop 就对应了一个选择器
                bootstrap.Group(eventLoop);
                //一个Channel都需要绑定到一个选择器（EventLoop）上
                bootstrap.Channel<TcpServerSocketChannel>();
                bootstrap.ChildHandler(new ActionChannelInitializer<IChannel>(channel =>
                {
                    //每一个选择器（EventLoop）和一个线程绑定
                    //IChannelPipeline pipeline = channel.Pipeline;
                    //pipeline.AddLast(new EchoServerHandler());
                }));
                IChannel boundChannel = await bootstrap.BindAsync(IPAddress.Loopback, 9000);
                Console.WriteLine("服务端已启动");
                Console.ReadLine();
                await boundChannel.CloseAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                await eventLoop.ShutdownGracefullyAsync();
            }
        }

    }
}

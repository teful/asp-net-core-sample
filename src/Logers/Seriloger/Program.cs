﻿using Serilog.Sinks.File.GZip;
using Serilog;
using Serilog.Sinks.File.Archive;
using System.IO.Compression;

namespace Seriloger
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                        .WriteTo.Async(a => a.File("logs/log-.txt", rollOnFileSizeLimit: true, retainedFileCountLimit: 10, rollingInterval: RollingInterval.Day, fileSizeLimitBytes: 104857600, outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss,fff} [{Level:u3}] ({SourceContext:l}/{ThreadId}) {Message}{NewLine}{Exception}", hooks: new ArchiveHooks(CompressionLevel.SmallestSize)))
                        .WriteTo.Async(a => a.Console(Serilog.Events.LogEventLevel.Debug))
                        .CreateLogger();

            Parallel.ForEach(Enumerable.Range(0, int.MaxValue / 2), i =>
            {
                Log.Information("{I}:{Id} An asynchronous wrapper for other Serilog sinks. Use this sink to reduce the overhead of logging calls by delegating work to a background thread. This is especially suited to non-batching sinks like the File and RollingFile sinks that may be affected by I/O bottlenecks.", i, Guid.NewGuid().ToString());
            });

            Console.WriteLine("Hello, World!");

            await Log.CloseAndFlushAsync();
        }
    }
}
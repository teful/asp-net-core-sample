﻿using System;

namespace DotnetInternalConfigureOptionsSample
{
    public class ConfigureNamedOptions<TOptions> where TOptions : class
    {
        public ConfigureNamedOptions(string name, Action<TOptions> action)
        {
            Name = name;
            Action = action;
        }

        public string Name { get; }

        public Action<TOptions> Action { get; }

        public virtual void Configure(string name, TOptions options)
        {
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            if (Name == null || name == Name)
            {
                Action?.Invoke(options);
            }
        }
    }
}

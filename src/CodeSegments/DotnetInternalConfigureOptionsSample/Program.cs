﻿using System;

namespace DotnetInternalConfigureOptionsSample
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            //.net core 中服务和中间件的配置例子，主要存在于 Program 和 Startup 中
            /*
             如：
             services.AddAuthentication(options =>
             {
                 options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                 options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
             })
             */

            {
                //添加配置
                var configure = new ConfigureNamedOptions<DefaultOptions>(nameof(DefaultOptions), options =>
                {
                    options.Id = 123;
                    options.Name = "Test";
                });

                //读取配置
                var defaultOptions = new DefaultOptions();
                configure.Action?.Invoke(defaultOptions);

                Console.WriteLine($"{nameof(DefaultOptions)}:{defaultOptions}");
            }
        }
    }
}

﻿namespace DotnetInternalConfigureOptionsSample
{
    class DefaultOptions
    {
        public int Id { get; set; } = 999;
        public string Name { get; set; } = "Default";

        public override string ToString()
        {
            return $"{this.Id}-{this.Name}";
        }
    }
}

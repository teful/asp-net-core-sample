﻿using System.Collections.Concurrent;

namespace BatchTaskQueue
{
    /// <summary>
    /// 任务队列
    /// </summary>
    internal class DefaultTaskQueue<T> : ITaskQueue<T>, IDisposable
    {
        /// <summary>
        /// 内置队列
        /// </summary>
        private ConcurrentDictionary<int, IList<T>> _taskQueues;
        /// <summary>
        /// 分区大小
        /// </summary>
        private readonly int _partitionSize;
        /// <summary>
        /// 默认index为0
        /// </summary>
        private int _index = 0;
        /// <summary>
        /// 默认处理偏移
        /// </summary>
        private int _offSet = 0;
        /// <summary>
        /// 内置锁
        /// </summary>
        private readonly object _lock = new();
        /// <summary>
        /// <summary>
        /// 构造
        /// </summary>
        /// <param name="partitionSize">分区大小，默认分区大小为10 </param>
        public DefaultTaskQueue(int partitionSize = 10)
        {
            _taskQueues = new ConcurrentDictionary<int, IList<T>>();
            _partitionSize = partitionSize;
            var ts = new List<T>();
            _taskQueues.AddOrUpdate(_index, ts, (k, v) => ts);
        }
        /// <summary>
        /// 增加一个对象
        /// </summary>
        /// <param name="t"></param>
        public void Add(T t)
        {
            lock (_lock)
            {
                if (_taskQueues.TryGetValue(_index, out var ts))
                {
                    if (ts.Count < _partitionSize)
                    {
                        ts.Add(t);
                        _taskQueues.AddOrUpdate(_index, ts, (k, v) => ts);
                    }
                    else //超出区域范围，则新建区
                    {
                        _index++;
                        IList<T> ts1 = new List<T>
                        {
                            t
                        };
                        _taskQueues.AddOrUpdate(_index, ts1, (k, v) => ts1);
                    }
                }
                else
                {
                    IList<T> ts1 = new List<T>
                    {
                        t
                    };
                    _taskQueues.AddOrUpdate(_index, ts1, (k, v) => ts1);
                }
            }
        }
        /// <summary>
        /// 获取一个分组队列
        /// </summary>
        /// <returns></returns>
        public IList<T>? GetQueue()
        {
            lock (_lock)
            {
                if (_taskQueues.TryGetValue(_offSet, out var ts))
                {
                    if (_offSet == _index)//如果直接获取一个能用的，那就新建区为新区
                    {
                        _index++;
                    }
                    return ts;
                }
                return default;
            }
        }
        /// <summary>
        /// 是否阻塞增加
        /// </summary>
        /// <returns></returns>
        public bool IsWaitAdd()
        {
            lock (_lock)
            {
                return _offSet != _index;
            }
        }
        /// <summary>
        /// 当前队列完成
        /// </summary>
        public void Complete()
        {
            lock (_lock)
            {
                _taskQueues.TryRemove(_offSet, out _);
                if (_offSet < _index)
                {
                    _offSet++;
                }
            }
        }

        public void Dispose()
        {
            if (_taskQueues != null)
            {
                _taskQueues.Clear();
            }
        }
    }
}

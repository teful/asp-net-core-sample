﻿namespace BatchTaskQueue
{
    internal class Program
    {
        static DefaultTaskQueue<string>? _taskQueue;
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");

            _taskQueue = new DefaultTaskQueue<string>(100);
            #region 一个一直生产指定大小，另外一个消费  先生产，再消费
            //for (int i = 0; i < 1000; i++)
            //{
            //    taskQueue.Add(i.ToString());
            //}
            //另外一个按照指定的时间，消费
            //while (true)
            //{
            //    Thread.Sleep(2000);
            //    Console.WriteLine("获取-----开始获取到数据!");
            //    var list = taskQueue.GetQueue();
            //    if (list != null)
            //    {
            //        Console.WriteLine($"获取-----对象状态：{taskQueue.IsWaitAdd()}已获取的队列列表:{string.Join(",", list)}");
            //        Console.WriteLine("获取-----处理1秒后，提交当前!");
            //        Thread.Sleep(1000);
            //        taskQueue.Complete();
            //        Console.WriteLine("获取-----已经提交!");
            //    }
            //}
            #endregion
            #region 两个任务处理，实现 一个生产，一个消费  批量  生产，并消费
            Task.Run(() =>
            {
                for (int i = 0; i < 10000000; i++)
                {
                    _taskQueue.Add(i.ToString());
                    Thread.Sleep(100);//一秒插入一条
                    Console.WriteLine($"插入-----队列状态：{_taskQueue.IsWaitAdd()}");
                    while (_taskQueue.IsWaitAdd())//有待处理任务
                    {
                        Console.WriteLine("插入-----任务插入中开始阻塞!");
                        SpinWait.SpinUntil(() => !_taskQueue.IsWaitAdd());
                    }
                }
            });

            while (true)
            {
                Thread.Sleep(2000);
                Console.WriteLine("获取-----开始获取到数据!");
                var list = _taskQueue.GetQueue();
                if (list != null)
                {
                    Console.WriteLine($"获取-----对象状态：{_taskQueue.IsWaitAdd()}已获取的队列列表:{string.Join(",", list)}");
                    Console.WriteLine("获取-----处理10秒后，提交当前!");
                    Thread.Sleep(1000);
                    _taskQueue.Complete();
                    Console.WriteLine("获取-----已经提交!");
                }
            }
            #endregion
        }
    }
}
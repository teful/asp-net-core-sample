﻿using CustomeDatabaseConfiguration.Models;
using Microsoft.EntityFrameworkCore;

namespace CustomeDatabaseConfiguration
{
    public class ConfigurationDbContext : DbContext
    {
        public ConfigurationDbContext(DbContextOptions options)
            : base(options)
        {
            base.Database.EnsureCreated();
        }
        public DbSet<ConfigurationEntity> ConfigurationEntities { get; set; }

    }
}

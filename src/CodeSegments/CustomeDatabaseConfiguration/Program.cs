﻿using System;
using System.Threading.Tasks;
using CustomeDatabaseConfiguration.ConfigurationProviders;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;

namespace CustomeDatabaseConfiguration
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var hostBuilder = Host.CreateDefaultBuilder(args)
               .ConfigureServices((services) =>
               {
                   services.TryAddScoped<TestClass>();
               }).ConfigureAppConfiguration((configure) =>
               {
                   var configSource = new EFConfigurationSource(options =>
                   {
                       options.UseSqlite($"DataSource={System.IO.Path.Combine(Environment.CurrentDirectory, "sample.db")}");
                   });
                   configure.Add(configSource);
               });

            var host = hostBuilder.Build();

            var test = host.Services.GetRequiredService<TestClass>();

            test.Show();

            await host.RunAsync();

        }
    }

    public class TestClass
    {
        private readonly IConfiguration configuration;

        public TestClass(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void Show()
        {

        }
    }
}

﻿namespace AuthorizationApiSample.Models
{
    public class ReqLoginUser
    {
        public string Email { get; set; }
        public string PassWord { get; set; }
        public bool RememberMe { get; set; }
    }
}

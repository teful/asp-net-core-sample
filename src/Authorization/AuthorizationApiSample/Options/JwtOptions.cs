﻿namespace AuthorizationApiSample.Options
{
    public class JwtOptions
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
        public int ExpireSeconds { get; set; }
    }
}

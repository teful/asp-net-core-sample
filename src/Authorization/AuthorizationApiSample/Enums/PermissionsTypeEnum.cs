﻿namespace AuthorizationApiSample.Enums
{
    /// <summary>
    /// 权限类型枚举
    /// </summary>
    public enum PermissionsTypeEnum : byte
    {
        Page = 1,
        Action
    }
}

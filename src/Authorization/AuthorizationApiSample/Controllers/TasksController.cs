﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthorizationApiSample.Controllers
{
    public class TasksController : RolePermissionsController
    {
        [HttpGet("Task1")]
        public string Task1()
        {
            return nameof(Task1);
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationApiSample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Policy = "RolePermissionsPolicy")]
    public class RolePermissionsController : ControllerBase
    {
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthorizationApiSample.Controllers
{
    public class UsersController : RolePermissionsController
    {
        [HttpGet("User1")]
        public string User1()
        {
            return nameof(User1);
        }
    }
}

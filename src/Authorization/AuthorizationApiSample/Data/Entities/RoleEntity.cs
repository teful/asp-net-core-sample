﻿using Microsoft.AspNetCore.Identity;

namespace AuthorizationApiSample.Data.Entities
{
    /// <summary>
    /// 角色实体
    /// </summary>
    public class RoleEntity : IdentityRole
    {
    }
}

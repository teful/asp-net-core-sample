﻿using AuthorizationApiSample.Enums;
using System;

namespace AuthorizationApiSample.Data.Entities
{
    /// <summary>
    /// 权限实体
    /// </summary>
    public class PermissionsEntity : IdentityPermissions<string>
    {
        public PermissionsEntity()
        {
            this.Id = Guid.NewGuid().ToString();
        }

        public PermissionsEntity(string permissionsName)
            : this()
        {
            this.Name = permissionsName;
        }

        public string ParentId { get; set; }

        public PermissionsTypeEnum Type { get; set; }

    }
}

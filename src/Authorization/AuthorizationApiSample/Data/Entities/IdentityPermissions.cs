﻿using System;

namespace AuthorizationApiSample.Data.Entities
{
    /// <summary>
    /// 身份权限
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public class IdentityPermissions<TKey> where TKey : IEquatable<TKey>
    {
        public IdentityPermissions()
        {
        }

        public IdentityPermissions(string permissionsName)
            : this()
        {
            Name = permissionsName;
        }

        public virtual TKey Id { get; set; }

        public virtual string Name { get; set; }

        public virtual string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();

        public virtual string ControllerName { get; set; }

        public virtual string ActionName { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}

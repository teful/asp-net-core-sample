﻿using Microsoft.AspNetCore.Identity;

namespace AuthorizationApiSample.Data.Entities
{
    /// <summary>
    /// 用户实体
    /// </summary>
    public class UserEntity : IdentityUser
    {
    }
}

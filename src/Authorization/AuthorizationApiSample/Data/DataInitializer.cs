﻿using AuthorizationApiSample.Data.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AuthorizationApiSample.Data
{
    public static class DataInitializer
    {
        public static async Task<IApplicationBuilder> UseDataInitializer(this IApplicationBuilder builder)
        {
            using (var scope = builder.ApplicationServices.CreateScope())
            {
                await MockDataAsync(scope);
            }
            return builder;
        }

        private static async Task MockDataAsync(IServiceScope scope)
        {
            var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
            var userManager = scope.ServiceProvider.GetService<UserManager<UserEntity>>();
            var roleManager = scope.ServiceProvider.GetService<RoleManager<RoleEntity>>();
            if (context.Users.Any())
            {
                return;
            }

            #region 用户

            var admin = new UserEntity
            {
                Email = "admin@test.com",
                UserName = "admin",
                EmailConfirmed = true,
            };
            await userManager.CreateAsync(admin, "abc.123");

            //身份标签
            await userManager.AddClaimsAsync(admin, new List<Claim>
            {
                new Claim("department","开发"),
            });

            var ordinary = new UserEntity
            {
                Email = "ordinary@test.com",
                UserName = "ordinary",
                EmailConfirmed = true
            };
            await userManager.CreateAsync(ordinary, "abc.123");

            //身份标签
            await userManager.AddClaimsAsync(ordinary, new List<Claim>
            {
                new Claim("department","销售"),
            });

            #endregion

            #region 角色
            var adminRole = new RoleEntity
            {
                Name = "admin"
            };
            await roleManager.CreateAsync(adminRole);

            var ordinaryRole = new RoleEntity
            {
                Name = "ordinary"
            };
            await roleManager.CreateAsync(ordinaryRole);
            #endregion

            await userManager.AddToRoleAsync(admin, adminRole.Name);
            await userManager.AddToRoleAsync(ordinary, ordinaryRole.Name);

            #region 权限

            #region 用户管理
            var userMP = new PermissionsEntity
            {
                Name = "用户管理",
                ControllerName = "Users",
                ActionName = "Index",
                Type = Enums.PermissionsTypeEnum.Page
            };
            context.Permissions.Add(userMP);
            context.SaveChanges();
            for (int i = 1; i < 5; i++)
            {
                context.Permissions.Add(new PermissionsEntity
                {
                    Name = $"用户管理{i}",
                    ParentId = userMP.Id,
                    ControllerName = "Users",
                    ActionName = $"Index{i}",
                    Type = Enums.PermissionsTypeEnum.Action
                });
            }
            context.SaveChanges();
            #endregion

            #region 任务管理
            var taskMP = new PermissionsEntity
            {
                Name = "任务管理",
                ControllerName = "Tasks",
                ActionName = "Index",
                Type = Enums.PermissionsTypeEnum.Page
            };
            context.Permissions.Add(taskMP);
            context.SaveChanges();
            for (int i = 1; i < 5; i++)
            {
                context.Permissions.Add(new PermissionsEntity
                {
                    Name = $"任务管理{i}",
                    ParentId = taskMP.Id,
                    ControllerName = "Tasks",
                    ActionName = $"Index{i}",
                    Type = Enums.PermissionsTypeEnum.Action
                });
            }
            context.SaveChanges();
            #endregion

            #endregion

            await context.RolePermissions.AddRangeAsync(new RolePermissionsEntity
            {
                PermissionsId = taskMP.Id,
                RoleId = adminRole.Id
            }, new RolePermissionsEntity
            {
                PermissionsId = userMP.Id,
                RoleId = adminRole.Id
            }, new RolePermissionsEntity
            {
                PermissionsId = taskMP.Id,
                RoleId = ordinaryRole.Id
            });
            await context.SaveChangesAsync();
        }


    }
}

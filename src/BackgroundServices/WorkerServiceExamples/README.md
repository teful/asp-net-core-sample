## 系统后台服务

1. ### 代码实现

   - 创建项目,引用服务支持包
   
     ```shell
     $dotnet new worker -o BackgrounService
     # Windows 系统
     $Install-Package Microsoft.Extensions.Hosting.WindowsServices
     # Linux 系统
     $Install-Package Microsoft.Extensions.Hosting.Systemd
     ```
   
   - 编码实现
   
     ```c#
     public class Program
     {
         public static void Main(string[] args)
         {
             CreateHost(args).Run();
         }
     
         private static IHost CreateHost(string[] args)
         {
             if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
             {
                 return CreateWindowsHostBuilder(args).Build();
             }
             else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
             {
                 return CreateLinuxHostBuilder(args).Build();
             }
     
             // 不支持平台(未测试)
             throw new PlatformNotSupportedException(RuntimeInformation.OSDescription);
         }
     
         /// <summary>
         /// 创建 Windows Host Builder
         /// </summary>
         /// <param name="args"></param>
         /// <returns></returns>
         private static IHostBuilder CreateWindowsHostBuilder(string[] args) =>
             CreateHostBuilder(args).UseWindowsService();
     
         /// <summary>
         /// 创建 Linux Host Builder
         /// </summary>
         /// <param name="args"></param>
         /// <returns></returns>
         private static IHostBuilder CreateLinuxHostBuilder(string[] args) =>
             CreateHostBuilder(args).UseSystemd();
     
         private static IHostBuilder CreateHostBuilder(string[] args) =>
             Host.CreateDefaultBuilder(args)
                 .ConfigureServices((hostContext, services) =>
                 {
                     services.AddHostedService<Worker>();
                 });
     }
     ```

2. ### 服务脚本

   - Windows => **bat**

     ```shell
     @echo off 
     @title 安装windows服务
     @echo off 
     echo= 发布服务程序
     dotnet publish -o Release -c Release -r win-x64 --self-contained false
     @echo off
     @sc create BackgrounService binPath= "%~dp0\Release\WorkerServiceExamples.exe"  
     echo= 启动服务...
     @echo off  
     @sc start BackgrounService 
     @echo off  
     echo= 配置服务...
     @echo off  
     @sc config BackgrounService start= AUTO  
     @echo off  
     echo= 成功安装、启动、配置服务...
     @pause
     ```

     > 中文会出现乱码: **ANSI** 编码保存

     运行结果:

     ```shell
     发布服务程序
     用于 .NET 的 Microsoft (R) 生成引擎版本 16.9.0+57a23d249
     版权所有(C) Microsoft Corporation。保留所有权利。
       正在确定要还原的项目…
       所有项目均是最新的，无法还原。
       WorkerServiceExamples -> D:\Study\AspNetCoreSample\src\BackgroundServices\WorkerServiceExamples\bin\Release\net5.0\win-x64\WorkerServiceExamples.dll
       WorkerServiceExamples -> D:\Study\AspNetCoreSample\src\BackgroundServices\WorkerServiceExamples\Release\
     [SC] CreateService 成功
      启动服务...
     
     SERVICE_NAME: BackgrounService
             TYPE               : 10  WIN32_OWN_PROCESS
             STATE              : 2  START_PENDING
                                     (NOT_STOPPABLE, NOT_PAUSABLE, IGNORES_SHUTDOWN)
             WIN32_EXIT_CODE    : 0  (0x0)
             SERVICE_EXIT_CODE  : 0  (0x0)
             CHECKPOINT         : 0x0
             WAIT_HINT          : 0x7d0
             PID                : 18456
             FLAGS              :
      配置服务...
     [SC] ChangeServiceConfig 成功
      成功安装、启动、配置服务...
     请按任意键继续. . .
     ```

   - Linux => **service**

     ```shell
     # 服务的描述和依赖关系
     [Unit]
     # 描述
     Description=Test Background Service
     
     # 服务的主要配置
     [Service]
     # 类型
     # Type=
     
     # 环境变量设置
     # Environment=
     
     # 设置工作文件夹路径
     WorkingDirectory=/home/dev3/workers/test
     
     # 启动
     ExecStart=/usr/bin/dotnet testworker.dll
     
     # 重启时执行
     # ExecReload=
     
     # 停止时执行
     # ExecStop=
     
     # 停止后执行
     # ExecStopPost=
     
     # 临时目录,是否分配独立的空间
     PrivateTmp=true
     StartLimitIntervalSec=0
     Restart=always
     RestartSec=1
     
     [Install]
     # 多用户方式启动
     WantedBy=multi-user.target
     ```
     
     > 以后缀名.service结尾,放置在 /etc/systemd/system/ 文件夹内
     
     批处理 => **sh**
     
     ```sh
     待补充
     ```
     
     运行结果:
     
     ```shell
     
     ```

3. ### 补充扩展

   - Windows 服务管理命令(需要使用管理员权限执行) [参考]([Sc | Microsoft Docs](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2012-R2-and-2012/cc754599(v=ws.11)?redirectedfrom=MSDN)

     ```shell
     $ sc create service_name binpath=service_path
     $ sc start service_name
     $ sc stop service_name
     $ sc delete service_name
     ```

   - Linux service 文件配置

   - Linux 服务管理命令
     ```shell
     $ systemctl start service_name.service #启动服务
     $ systemctl restart service_name.service #重启服务
     $ systemctl stop service_name.service #停止服务
     $ systemctl kill service_name.service #杀死服务的所有子进程
     $ systemctl status service_name.service #查看服务状态
     $ systemctl enable service_name.service #设置开机自启
     $ systemctl disable service_name.service #禁用开机自启
     $ systemctl is-enabled service_name.service #查看是开机自启
     $ systemctl list-units --type=service #查看服务列表
     ```




using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Runtime.InteropServices;

namespace WorkerServiceExamples
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHost(args).Run();
        }

        private static IHost CreateHost(string[] args)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return CreateWindowsHostBuilder(args).Build();
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                return CreateLinuxHostBuilder(args).Build();
            }

            throw new PlatformNotSupportedException(RuntimeInformation.OSDescription);
        }

        /// <summary>
        /// ���� Windows Host Builder
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private static IHostBuilder CreateWindowsHostBuilder(string[] args) =>
            CreateHostBuilder(args).UseWindowsService();

        /// <summary>
        /// ���� Linux Host Builder
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private static IHostBuilder CreateLinuxHostBuilder(string[] args) =>
            CreateHostBuilder(args).UseSystemd();

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<Worker>();
                });
    }
}

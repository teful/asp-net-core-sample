@echo off 
@title 安装windows服务
@echo off 
echo= 发布服务程序
dotnet publish -o Release -c Release -r win-x64 --self-contained false
@echo off
@sc create BackgrounService binPath= "%~dp0\Release\WorkerServiceExamples.exe"  
echo= 启动服务...
@echo off  
@sc start BackgrounService 
@echo off  
echo= 配置服务...
@echo off  
@sc config BackgrounService start= AUTO  
@echo off  
echo= 成功安装、启动、配置服务...
@pause
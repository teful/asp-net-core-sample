﻿using BenchmarkDotNet.Running;
using ContentHolder;

namespace BenchmarkContentHolder
{
    internal class Program
    {
        static void Main(string[] args)
        {

#if !DEBUG
            BenchmarkRunner.Run<CombinKeywordMatcherTest>();
            BenchmarkRunner.Run<SingleKeywordMatcherTest>();
#else
            {
                //var test = new CombinKeywordMatcherTest();
                //foreach (var item in test.MatchSource)
                //{
                //    Console.WriteLine(test.Match(item));
                //}
            }
            {
                var matcher = new SingleKeywordMatcher(["天子中支", "天子细支", "天下无敌", "天空晴朗"]);
                matcher.Matching("天空清凉");
            }
#endif

        }
    }
}

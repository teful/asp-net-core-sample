﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using ContentHolder;

namespace BenchmarkContentHolder
{
    [SimpleJob(RuntimeMoniker.Net80)]
    [HideColumns("Error", "StdDev", "RatioSD")]
    public class CombinKeywordMatcherTest
    {
        const string COMBIN_SOURCE_PATH = @"C:\Users\dev122\Desktop\combin_keywords.txt";

        public IEnumerable<string> MatchSource
        {
            get
            {
                yield return "【蒙策咨询】辅佐本地餐饮商入驻抖音外卖，1~3个工作日即可开通成功 回复1了解详情，拒收请回复R";
            }
        }

        private readonly IKeywordMatcher _keywordMatcher;
        private readonly IKeywordMatcher _regexMatcher;

        public CombinKeywordMatcherTest()
        {
            var keywords = File.ReadAllLines(COMBIN_SOURCE_PATH);
            _keywordMatcher = new CombinKeywordMatcher(keywords);
            _regexMatcher = new CombinKeywordRegexMatcher(keywords);
        }

        [Benchmark(Baseline = true)]
        [ArgumentsSource(nameof(MatchSource))]
        public int Match(string source)
        {
            var ressult = this._keywordMatcher.Matching(source);
            return ressult.Count();
        }

        [Benchmark]
        [ArgumentsSource(nameof(MatchSource))]
        public int RegexMatch(string source)
        {
            var ressult = this._regexMatcher.Matching(source);
            return ressult.Count();
        }
    }
}

﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using ContentHolder;

namespace BenchmarkContentHolder
{
    [SimpleJob(RuntimeMoniker.Net80)]
    [HideColumns("Error", "StdDev", "RatioSD")]
    public class SingleKeywordMatcherTest
    {
        const string SINGLE_SOURCE_PATH = @"C:\Users\dev122\Desktop\keywords.txt";

        public IEnumerable<string> MatchSource
        {
            get
            {
                yield return "【蒙策咨询】辅佐本地餐饮商入驻抖音外卖，1~3个工作日即可开通成功 回复1了解详情，拒收请回复R";
            }
        }

        private readonly IKeywordMatcher _keywordMatcher;
        private readonly IKeywordMatcher _xMatcher;

        public SingleKeywordMatcherTest()
        {
            var keywords = File.ReadAllLines(SINGLE_SOURCE_PATH);
            this._keywordMatcher = new SingleKeywordMatcher(keywords);
            this._xMatcher = new SingleKeywordXMatcher(keywords);
        }

        [Benchmark(Baseline = true)]
        [ArgumentsSource(nameof(MatchSource))]
        public int Match(string source)
        {
            var ressult = this._keywordMatcher.Matching(source);
            return ressult.Count();
        }

        [Benchmark]
        [ArgumentsSource(nameof(MatchSource))]
        public int XMatch(string source)
        {
            var ressult = this._xMatcher.Matching(source);
            return ressult.Count();
        }

    }
}

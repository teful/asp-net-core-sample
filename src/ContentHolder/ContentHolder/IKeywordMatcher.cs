﻿namespace ContentHolder
{
    public interface IKeywordMatcher
    {
        IEnumerable<string> Matching(string source);
    }
}

﻿using System;

namespace Example
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var msgContext = new MessageContext { };

            var builder = MessagePipelineBuilder.Create<MessageContext>(context =>
              {
                  Console.WriteLine($"{DateTime.Now:t} 处理完成");
              })
              //常规检测
              .Use((context, next) =>
              {
                  Console.WriteLine($"{DateTime.Now:t} 常规检测 开始");
                  if (DateTime.Now.Millisecond % 2 == 0)
                  {
                      Console.WriteLine($"{DateTime.Now:t} 处理常规检测不通过");
                  }
                  else
                  {
                      next();
                  }
                  Console.WriteLine($"{DateTime.Now:t} 常规检测 结束");
              })
              //内容
              .Use((context, next) =>
              {
                  Console.WriteLine($"{DateTime.Now:t} 内容 开始");
                  if (DateTime.Now.Millisecond % 3 == 0)
                  {
                      Console.WriteLine($"{DateTime.Now:t} 处理内容失败");
                  }
                  else
                  {
                      next();
                  }
                  Console.WriteLine($"{DateTime.Now:t} 内容 结束");
              })
              //号码
              .Use((context, next) =>
              {
                  Console.WriteLine($"{DateTime.Now:t} 号码 开始");
                  if (DateTime.Now.Millisecond % 4 == 0)
                  {
                      Console.WriteLine($"{DateTime.Now:t} 处理号码失败");
                  }
                  else
                  {
                      next();
                  }
                  Console.WriteLine($"{DateTime.Now:t} 号码 结束");
              })
              //路由
              .Use((context, next) =>
              {
                  Console.WriteLine($"{DateTime.Now:t} 路由 开始");
                  if (DateTime.Now.Millisecond % 4 == 0)
                  {
                      Console.WriteLine($"{DateTime.Now:t} 处理路由失败");
                  }
                  else
                  {
                      next();
                  }
                  Console.WriteLine($"{DateTime.Now:t} 路由 开始");
              })
              //通道
              .Use((context, next) =>
              {
                  Console.WriteLine($"{DateTime.Now:t} 通道 结束");
                  if (DateTime.Now.Millisecond % 4 == 0)
                  {
                      Console.WriteLine($"{DateTime.Now:t} 处理通道失败");
                  }
                  else
                  {
                      next();
                  }
                  Console.WriteLine($"{DateTime.Now:t} 通道 结束");
              });

            var pipeline = builder.Build();

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine();
                Console.WriteLine($"*****{i}*****");
                pipeline?.Invoke(msgContext);
                Console.WriteLine($"*****{i}*****");
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}

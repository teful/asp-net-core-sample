﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Example.MessagePipelines
{
    /// <summary>
    /// 消息同步管道构建器接口
    /// </summary>
    /// <typeparam name="MsgContext"></typeparam>
    public interface IMessagePipelineBuilder<MsgContext>
    {
        IMessagePipelineBuilder<MsgContext> Use(Func<Action<MsgContext>, Action<MsgContext>> middleware);

        Action<MsgContext> Build();
    }

    /// <summary>
    /// 消息同步管道构建器实现
    /// </summary>
    /// <typeparam name="MsgContext"></typeparam>
    public class MessagePipelineBuilder<MsgContext> : IMessagePipelineBuilder<MsgContext>
    {
        private readonly Action<MsgContext> _completeFunc;

        private readonly IList<Func<Action<MsgContext>, Action<MsgContext>>> _components;

        public MessagePipelineBuilder(Action<MsgContext> completeFunc)
        {
            this._completeFunc = completeFunc;
            this._components = new List<Func<Action<MsgContext>, Action<MsgContext>>>();
        }

        public Action<MsgContext> Build()
        {
            var request = this._completeFunc;

            //按照注入的顺序，遍历执行
            foreach (var component in this._components.Reverse())
            {
                request = component(request);
            }

            return request;
        }

        public IMessagePipelineBuilder<MsgContext> Use(Func<Action<MsgContext>, Action<MsgContext>> middleware)
        {
            this._components.Add(middleware);
            return this;
        }
    }
}

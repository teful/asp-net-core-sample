﻿using Example.MessagePipelines;
using System;
using System.Threading.Tasks;

namespace Example
{
    public static class Extension
    {
        /// <summary>
        /// 消息处理同步管道
        /// </summary>
        /// <typeparam name="MsgContext"></typeparam>
        /// <param name="builder"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static IMessagePipelineBuilder<MsgContext> Use<MsgContext>(this IMessagePipelineBuilder<MsgContext> builder, Action<MsgContext, Action> action)

        {
            return builder.Use(next =>
                context => action(context, () => next(context)));
        }

        /// <summary>
        /// 消息处理异步管道
        /// </summary>
        /// <typeparam name="MsgContext"></typeparam>
        /// <param name="builder"></param>
        /// <param name="func"></param>
        /// <returns></returns>
        public static IMessageAsyncPipelineBuilder<MsgContext> Use<MsgContext>(this MessageAsyncPipelineBuilder<MsgContext> builder, Func<MsgContext, Func<Task>, Task> func)
        {
            return builder.Use(next =>
                context => func(context, () => next(context)));
        }
    }
}

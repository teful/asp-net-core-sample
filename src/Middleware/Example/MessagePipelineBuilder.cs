﻿using Example.MessagePipelines;
using System;
using System.Threading.Tasks;

namespace Example
{
    public class MessagePipelineBuilder
    {
        public static IMessagePipelineBuilder<MsgContext> Create<MsgContext>(Action<MsgContext> completeAction)
        {
            return new MessagePipelineBuilder<MsgContext>(completeAction);
        }

        public static IMessageAsyncPipelineBuilder<MsgContext> CreateAsync<MsgContext>(Func<MsgContext, Task> completeFunc)
        {
            return new MessageAsyncPipelineBuilder<MsgContext>(completeFunc);
        }
    }
}

﻿namespace PluginConsoleApp
{
    internal class PluginLoadContext : LazyPluginLoadContext, IPluginContext
    {
        public PluginLoadContext(string pluginId, string pluginMainDllFilePath) 
            : base(pluginId, pluginMainDllFilePath)
        {
        }
    }
}

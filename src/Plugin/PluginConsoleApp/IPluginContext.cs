﻿using System.Reflection;

namespace PluginConsoleApp
{
    public interface IPluginContext
    {
        string PluginId { get; }

        IEnumerable<Assembly> Assemblies { get; }

        Assembly? LoadFromAssemblyName(AssemblyName assemblyName);

        void Unload();
    }
}

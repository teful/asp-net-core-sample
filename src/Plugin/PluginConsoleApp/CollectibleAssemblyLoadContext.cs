﻿using System.Reflection;
using System.Runtime.Loader;

namespace PluginConsoleApp
{
    internal class CollectibleAssemblyLoadContext : AssemblyLoadContext, IPluginContext
    {
        public string PluginId
        {
            get
            {
                return this.Name ?? "";
            }
        }

        public CollectibleAssemblyLoadContext(string? name)
             : base(isCollectible: true, name: name)
        {
        }

        protected override Assembly? Load(AssemblyName name) => default(Assembly);
    }
}

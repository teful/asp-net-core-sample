﻿using PluginCore;

namespace TestPlugin
{
    public class TestPluginCore : IPluginCore
    {
        const string VERSION = "v1.1";
        public string Running(string args)
        {
            Console.WriteLine($"{nameof(TestPluginCore)} {VERSION} : args = {args}");

            return VERSION;
        }
    }
}
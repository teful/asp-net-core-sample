﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BenchmarkDotNetSample
{
    /// <summary>
    /// 号段匹配测试
    /// </summary>
    [SimpleJob(RuntimeMoniker.Net80)]
    [SimpleJob(RuntimeMoniker.Net70)]
    [SimpleJob(RuntimeMoniker.Net60)]
    [SimpleJob(RuntimeMoniker.Net50)]
    [RPlotExporter]
    public class NumSegmentMatchTest
    {
        private static readonly List<NumSegment<string>> _strSegments;
        private static readonly List<NumSegment<ushort>> _shortSegments;
        private static readonly HashSet<HashSetNumSegment<ushort>> _hashShortSegments;
        private static readonly List<NumSegmentModel> _segmentModels;
        static NumSegmentMatchTest()
        {
            Console.WriteLine("开始初始化数据");
            _strSegments = new List<NumSegment<string>>();
            _shortSegments = new List<NumSegment<ushort>>();
            _segmentModels = new List<NumSegmentModel>();
            _hashShortSegments = new HashSet<HashSetNumSegment<ushort>>();
            for (ushort i = 100; i < 200; i++)
            {
                _strSegments.Add(new NumSegment<string>
                {
                    PrefixCode = i.ToString(),
                    NumSegments = Enumerable.Range(0, 10000).Select(v => new NumSegment<string>
                    {
                        PrefixCode = v.ToString().PadLeft(4, '0'),
                        OperatorId = v % 2,
                        RegionId = v % 3
                    }).ToArray()
                });

                var list = Enumerable.Range(0, 10000).Select(v => new NumSegmentModel
                {
                    NumSegment = $"{i}{v.ToString().PadLeft(4, '0')}",
                    OperatorId = v % 2,
                    RegionId = v % 3
                });

                _segmentModels.AddRange(list);

                _shortSegments.Add(new NumSegment<ushort>
                {
                    PrefixCode = i,
                    NumSegments = Enumerable.Range(0, 10000).Select(v => new NumSegment<ushort>
                    {
                        PrefixCode = (ushort)v,
                        OperatorId = v % 2,
                        RegionId = v % 3
                    }).ToArray()
                });

                var numSegment = new HashSetNumSegment<ushort>
                {
                    PrefixCode = i,
                    NumSegments = new HashSet<HashSetNumSegment<ushort>>()
                };

                Enumerable.Range(0, 10000).Select(v => new HashSetNumSegment<ushort>
                {
                    PrefixCode = (ushort)v,
                    OperatorId = v % 2,
                    RegionId = v % 3
                }).ToList().ForEach(item =>
                {
                    numSegment.NumSegments.Add(item);
                });

                _hashShortSegments.Add(numSegment);
            }
            Console.WriteLine($"初始化数据完成");
        }
        public static void Show()
        {

        }
        public IEnumerable<ulong> GetSources()
        {
            //yield return 13000000000;
            //yield return 13000000010;
            //yield return 13000000045;
            yield return 13000000046;
        }

        [Benchmark(Description = "分段比较整段匹配测试(string)")]
        [ArgumentsSource(nameof(GetSources))]
        public NumSegment<string> SegmentMatchingTest(ulong number)
        {
            var numSegment = number / 10000;

            var threeNum = (numSegment / 10000).ToString();
            var result = _strSegments.FirstOrDefault(segment => segment.PrefixCode == threeNum);
            if (result.NumSegments == default)
            {
                return default;
            }
            var fourNum = (numSegment % 10000).ToString().PadLeft(4, '0');
            return result.NumSegments.FirstOrDefault(segment => segment.PrefixCode == fourNum);
        }


        [Benchmark(Description = "分段比较整段匹配测试(ushort)")]
        [ArgumentsSource(nameof(GetSources))]
        public NumSegment<ushort> UshortSegmentMatchingTest(ulong number)
        {
            var numSegment = number / 10000;

            var threeNum = numSegment / 10000;
            var result = _shortSegments.FirstOrDefault(segment => segment.PrefixCode == threeNum);
            if (result.NumSegments == default)
            {
                return default;
            }
            var fourNum = numSegment % 10000;
            return result.NumSegments.FirstOrDefault(segment => segment.PrefixCode == fourNum);
        }

        [Benchmark(Description = "HashSet分段比较整段匹配测试(ushort)")]
        [ArgumentsSource(nameof(GetSources))]
        public HashSetNumSegment<ushort> HashUshortSegmentMatchingTest(ulong number)
        {
            var numSegment = number / 10000;

            var threeNum = numSegment / 10000;
            var result = _hashShortSegments.FirstOrDefault(segment => segment.PrefixCode == threeNum);
            if (result.NumSegments == default)
            {
                return default;
            }
            var fourNum = numSegment % 10000;
            return result.NumSegments.FirstOrDefault(segment => segment.PrefixCode == fourNum);
        }

        static NumSegment<string> Matching(IEnumerable<NumSegment<string>> numSegments, string prefixCode)
        {
            if (numSegments == default)
            {
                return default;
            }
            foreach (var item in numSegments)
            {
                int i;
                for (i = 0; i < item.PrefixCode.Length; i++)
                {
                    if (item.PrefixCode[i] != prefixCode[i])
                    {
                        break;
                    }
                }
                if (i == prefixCode.Length)
                {
                    return item;
                }
            }
            return default;
        }

        [Benchmark(Description = "分段比较字符匹配测试")]
        [ArgumentsSource(nameof(GetSources))]
        public NumSegment<string> CompareCharBySegmentMatchingTest(ulong number)
        {
            var numSegment = number / 10000;

            var threeNum = (numSegment / 10000).ToString();
            var result = Matching(_strSegments, threeNum);

            var fourNum = (numSegment % 10000).ToString().PadLeft(4, '0');
            return Matching(result.NumSegments, fourNum);
        }

        [Benchmark(Description = "比较字符匹配测试")]
        [ArgumentsSource(nameof(GetSources))]
        public NumSegmentModel CompareCharMatchingTest(ulong number)
        {
            var numberStr = number.ToString();
            foreach (var model in _segmentModels)
            {
                int i;
                for (i = 0; i < model.NumSegment.Length; i++)
                {
                    if (model.NumSegment[i] != numberStr[i])
                    {
                        break;
                    }
                }
                if (i == model.NumSegment.Length)
                {
                    return model;
                }
            }

            return default;
        }

        [Benchmark(Description = "比较整个号码字符串匹配测试")]
        [ArgumentsSource(nameof(GetSources))]
        public NumSegmentModel CompareNumMatchingTest(ulong number)
        {
            var numberStr = number.ToString();
            return _segmentModels.FirstOrDefault(segment => numberStr.StartsWith(segment.NumSegment));
        }
    }

    public struct NumSegment<T>
    {
        public T PrefixCode { get; set; }
        public int? OperatorId { get; set; }
        public int? RegionId { get; set; }
        public NumSegment<T>[] NumSegments { get; set; }
    }

    public struct HashSetNumSegment<T>
    {
        public T PrefixCode { get; set; }
        public int? OperatorId { get; set; }
        public int? RegionId { get; set; }
        public HashSet<HashSetNumSegment<T>> NumSegments { get; set; }
    }
    public struct NumSegmentModel
    {
        public string NumSegment { get; set; }
        public int OperatorId { get; set; }
        public int RegionId { get; set; }
    }
}

### 使用步骤

1. 添加包

   ```shell
   dotnet add package BenchmarkDotNet
   ```

2. 标记测试方法(类和方法必须是public)

   ```c#
   public class RegexTest
   {
   	private Regex _regex = new Regex("[a-zA-Z0-9]*", RegexOptions.Compiled);
   
   	[Benchmark]
   	public bool IsMatch() => _regex.IsMatch("abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ");
   }
   ```

3. 引用测试

   ```c#
   BenchmarkRunner.Run<RegexTest>();
   ```

4. 执行(必须在release模式下执行)

   ```shell
   dotnet run -c release -f net5.0
   ```

5. 结果

   ```shell
   BenchmarkDotNet=v0.13.0, OS=Windows 10.0.19041.985 (2004/May2020Update/20H1)
   Intel Core i5-8250U CPU 1.60GHz (Kaby Lake R), 1 CPU, 8 logical and 4 physical cores
   .NET SDK=5.0.203
     [Host]   : .NET 5.0.6 (5.0.621.22011), X64 RyuJIT
     .NET 5.0 : .NET 5.0.6 (5.0.621.22011), X64 RyuJIT
   
   Job=.NET 5.0  Runtime=.NET 5.0
   
   |  Method |     Mean |   Error |  StdDev |
   |-------- |---------:|--------:|--------:|
   | IsMatch | 196.1 ns | 1.78 ns | 1.49 ns |
   ```

### 其他用法

- 多平台

  1. 代码标记

     ```c#
     [SimpleJob(RuntimeMoniker.Net50)]
     [SimpleJob(RuntimeMoniker.NetCoreApp31)]
     [SimpleJob(RuntimeMoniker.Net472)]
     public class RegexTest
     {
     	private Regex _regex = new Regex("[a-zA-Z0-9]*", RegexOptions.Compiled);
     
     	[Benchmark]
     	public bool IsMatch() => _regex.IsMatch("abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ");
     }
     ```

  2. 项目文件设置

     ```xml
     <TargetFrameworks>net5.0;netcoreapp3.1;net472</TargetFrameworks>
     ```

  ------

  ```shell
  dotnet run -c release -f net5.0
  BenchmarkDotNet=v0.13.0, OS=Windows 10.0.19041.985 (2004/May2020Update/20H1)
  Intel Core i5-8250U CPU 1.60GHz (Kaby Lake R), 1 CPU, 8 logical and 4 physical cores
  .NET SDK=5.0.203
    [Host]               : .NET 5.0.6 (5.0.621.22011), X64 RyuJIT
    .NET 5.0             : .NET 5.0.6 (5.0.621.22011), X64 RyuJIT
    .NET Core 3.1        : .NET Core 3.1.15 (CoreCLR 4.700.21.21202, CoreFX 4.700.21.21402), X64 RyuJIT
    .NET Framework 4.7.2 : .NET Framework 4.8 (4.8.4341.0), X64 RyuJIT
  
  |  Method |                  Job |              Runtime |       Mean |    Error |   StdDev |
  |-------- |--------------------- |--------------------- |-----------:|---------:|---------:|
  | IsMatch |             .NET 5.0 |             .NET 5.0 |   198.8 ns |  3.99 ns |  4.75 ns |
  | IsMatch |        .NET Core 3.1 |        .NET Core 3.1 |   838.9 ns | 14.84 ns | 13.16 ns |
  | IsMatch | .NET Framework 4.7.2 | .NET Framework 4.7.2 | 1,027.4 ns | 13.71 ns | 10.70 ns |
  ```

- 输入参数

  ```c#
  [Benchmark]
  [Arguments("abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")]
  [Arguments("ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789abcdefghijklmnopqrstuvwxyz")]
  [ArgumentsSource(nameof(GetSources))]
  public bool IsMatch(string source) => _regex.IsMatch(source);
  
  public IEnumerable<string> GetSources()
  {
  	yield return "abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  	yield return "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789abcdefghijklmnopqrstuvwxyz";
  }
  ```

### 资料

[GitHub - dotnet/BenchmarkDotNet: Powerful .NET library for benchmarking](https://github.com/dotnet/BenchmarkDotNet)


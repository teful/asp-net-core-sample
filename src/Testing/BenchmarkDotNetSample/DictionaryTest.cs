﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace BenchmarkDotNetSample
{
    //[SimpleJob(RuntimeMoniker.NativeAot80)]
    [SimpleJob(RuntimeMoniker.Net80)]
    [SimpleJob(RuntimeMoniker.Net70, baseline: true)]
    [SimpleJob(RuntimeMoniker.Net60)]
    [HideColumns("Error", "StdDev", "RatioSD")]
    public class DictionaryTest
    {
        private readonly Dictionary<int, string> _data;

        public DictionaryTest()
        {
            _data = new Dictionary<int, string>();
            for (int i = 0; i < 60; i++)
            {
                _data[i] = i.ToString();
            }
        }

        public IEnumerable<object[]> Keys
        {
            get
            {
                yield return new object[] { 10 };
                //yield return new object[] { 20 };
                yield return new object[] { 60 };
            }
        }

        [Benchmark(Baseline = true)]
        [ArgumentsSource(nameof(Keys))]
        public bool GeneralTryGetValue(int key)
        {
            return this._data.TryGetValue(key, out _);
        }

        [Benchmark]
        [ArgumentsSource(nameof(Keys))]
        public bool UnusualTryGetValue1(int key)
        {
            return CollectionsMarshal.GetValueRefOrNullRef(this._data, key) != default;
        }

        [Benchmark]
        [ArgumentsSource(nameof(Keys))]
        public bool UnusualTryGetValue2(int key)
        {
            CollectionsMarshal.GetValueRefOrAddDefault(this._data, key, out var exists);
            this._data.Remove(key);
            return exists;
            //return CollectionsMarshal.GetValueRefOrNullRef(this._data, key) != default;
        }

        //[Benchmark]
        public void GeneralAddValue(int key, string value)
        {
            this._data[key] = value;
        }

        //[Benchmark]
        public void UnusualAddValue(int key, string value)
        {
            CollectionsMarshal.GetValueRefOrAddDefault(this._data, key, out var exists);
            if (!exists)
            {
                this._data[key] = value;
            }
        }

    }
}

﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using System;

namespace BenchmarkDotNetSample
{
    [SimpleJob(RuntimeMoniker.Net80)]
    [SimpleJob(RuntimeMoniker.Net70, baseline: true)]
    [SimpleJob(RuntimeMoniker.Net60)]
    public class CurrentTimeTest
    {
        [Benchmark]
        public DateTime GetTimeByDateTime()
        {
            return DateTime.Now;
        }

        [Benchmark]
        public DateTimeOffset GetTimeByDateTimeOffset()
        {
            return DateTimeOffset.Now;
        }

        [Benchmark]
        public DateTime GetUTCTimeByDateTime()
        {
            return DateTime.UtcNow;
        }

        [Benchmark]
        public DateTimeOffset GetUTCTimeByDateTimeOffset()
        {
            return DateTimeOffset.UtcNow;
        }
    }
}

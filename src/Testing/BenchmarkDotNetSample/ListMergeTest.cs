﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace BenchmarkDotNetSample
{
    [SimpleJob(RuntimeMoniker.Net80)]
    [SimpleJob(RuntimeMoniker.Net70)]
    [SimpleJob(RuntimeMoniker.Net60)]
    [HideColumns("Error", "StdDev", "RatioSD")]
    public class ListMergeTest
    {
        public IEnumerable<object[]> IListSource
        {
            get
            {
                yield return new object[] { new List<InterceptorValue>(), new List<InterceptorValue>(), new List<InterceptorValue>(), new List<InterceptorValue>() };
            }
        }

        [Benchmark(Baseline = true)]
        public string GetIList()
        {
            const string ACCOUNT_KEY = "account";
            const string CHANNEL_KEY = "channel";

            var result = new object[]
            {
                new
                {
                    Type = ACCOUNT_KEY,
                    Value = Array.Empty<InterceptorValue>()
                },
                new
                {
                    Type = CHANNEL_KEY,
                    Value = Array.Empty<InterceptorValue>()
                }
            };

            return JsonSerializer.Serialize(result);
        }

        public IEnumerable<object[]> IEnumerableSource
        {
            get
            {
                yield return new object[] { Array.Empty<InterceptorValue>(), Array.Empty<InterceptorValue>(), Array.Empty<InterceptorValue>(), Array.Empty<InterceptorValue>() };
            }
        }

        [Benchmark]
        public string GetIEnumerable()
        {
            const string ACCOUNT_KEY = "account";
            const string CHANNEL_KEY = "channel";

            var result = new object[]
            {
                new
                {
                    Type = ACCOUNT_KEY,
                    Value = new InterceptorValue[]{ new("123","456",new string[] { "789", "测试" }) }
                },
                new
                {
                    Type = CHANNEL_KEY,
                    Value = new InterceptorValue[]{ new("123","456",new string[] { "789", "测试" }) }
                }
            };

            return JsonSerializer.Serialize(result);
        }

        public struct InterceptorValue
        {
            // interceptorId blackListPool ...
            public string Id { get; }

            // keyword black ...
            public string Type { get; }

            public string[] Value { get; }

            public InterceptorValue(string id, string type, params string[] value)
            {
                Id = id;
                Type = type;
                Value = value;
            }
        }
    }
}

﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using System;

namespace BenchmarkDotNetSample
{
    [SimpleJob(RuntimeMoniker.Net80)]
    [SimpleJob(RuntimeMoniker.Net70)]
    [SimpleJob(RuntimeMoniker.Net60)]
    public class IdGeneratorTest
    {
        [Benchmark]
        public long DateTimeOffsetUtcNow()
        {
            var idGenerator = new SnowFlakeIdGenerator(10)
            {
                GetTimestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds
            };

            return idGenerator.NextId();
        }

        [Benchmark]
        public long DateTimeUtcNow()
        {
            var idGenerator = new SnowFlakeIdGenerator(10)
            {
                GetTimestamp = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeMilliseconds
            };

            return idGenerator.NextId();
        }

        [Benchmark]
        public long DateTimeOffsetNow()
        {
            var idGenerator = new SnowFlakeIdGenerator(10)
            {
                GetTimestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds
            };

            return idGenerator.NextId();
        }

        [Benchmark]
        public long DateTimeNow()
        {
            var idGenerator = new SnowFlakeIdGenerator(10)
            {
                GetTimestamp = ((DateTimeOffset)DateTime.Now).ToUnixTimeMilliseconds
            };

            return idGenerator.NextId();
        }

        internal sealed class SnowFlakeIdGenerator
        {
            #region 机器

            /// <summary>
            /// 机器ID
            /// </summary>
            private long _machineId;

            /// <summary>
            /// 机器ID字节数
            /// </summary>
            private const long MachineIdBits = 5L;

            /// <summary>
            /// 支持的最大机器ID，最大为31
            /// </summary>
            private const long MaxMachineId = -1L ^ -1L << (int)MachineIdBits;

            #endregion

            #region 数据

            /// <summary>
            /// 数据id
            /// </summary>
            private long _dataId;

            /// <summary>
            /// 数据标识ID所占的字节数
            /// </summary>
            private const long DataBits = 5L;

            /// <summary>
            /// 支持的最大数据id，最大为31
            /// </summary>
            private const long MaxDataBitId = -1L ^ (-1L << (int)DataBits);

            #endregion

            /// <summary>
            /// 序列在 ID 中占的位数
            /// </summary>
            private const long SequenceBits = 12L;

            /// <summary>
            /// 机器 ID 向左移12位
            /// </summary>
            private const long MachineIdShift = SequenceBits;

            /// <summary>
            /// 数据 ID 向左移17位
            /// </summary>
            private const long DatacenterIdShift = SequenceBits + MachineIdBits;

            /// <summary>
            /// 时间截向左移22位
            /// </summary>
            private const long TimestampLeftShift = DatacenterIdShift + DataBits;

            /// <summary>
            /// 生成序列的掩码最大值，最大为4095
            /// </summary>
            private const long SequenceMask = -1L ^ -1L << (int)SequenceBits;

            /// <summary>
            /// 毫秒内序列(0~4095)
            /// </summary>
            private long _sequence;

            /// <summary>
            /// 上次时间戳
            /// </summary>
            private long _lastTimestamp = -1L;

            /// <summary>
            /// 开始时间戳，单位毫秒(2023-01-01 00:00:00)
            /// </summary>
            private const long Twepoch = 1672502400000L;

            private static readonly object _idLocker = new object();

            /// <summary>
            /// 
            /// </summary>
            /// <param name="workerId">使用工作机器的序号(也就是将机房的去掉给机器ID使用)，范围是 [0, 1023]，优点是方便给机器编号</param>
            public SnowFlakeIdGenerator(long workerId)
            {
                // 计算最大值
                var maxMachineId = (MaxDataBitId + 1) * (MaxMachineId + 1) - 1;

                if (workerId < 0 || workerId > maxMachineId)
                {
                    throw new ArgumentOutOfRangeException($"Worker ID can't be greater than {maxMachineId} or less than 0");
                }

                // 取高位部分作为数据ID部分
                _dataId = (workerId >> (int)MachineIdBits) & MaxDataBitId;

                // 取低位部分作为机器ID部分
                _machineId = workerId & MaxMachineId;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="dataId">数据id(0~31)</param>
            /// <param name="machineId">机器id(0~31)</param>
            public SnowFlakeIdGenerator(long dataId, long machineId)
            {
                if (machineId is > MaxMachineId or < 0)
                {
                    throw new ArgumentOutOfRangeException($"Machine ID can't be greater than {machineId} or less than 0");
                }

                if (dataId is > MaxDataBitId or < 0)
                {
                    throw new ArgumentOutOfRangeException($"Data ID can't be greater than {dataId} or less than 0");
                }

                _machineId = machineId;
                _dataId = dataId;
            }

            /// <summary>
            /// 获取下一个id
            /// </summary>
            /// <returns></returns>
            public long NextId()
            {
                lock (_idLocker)
                {
                    var timestamp = GetTimestamp();
                    // 如果当前时间小于上一次 ID 生成的时间戳，说明发生时钟回拨，为保证ID不重复抛出异常。
                    if (timestamp < _lastTimestamp)
                    {
                        throw new ArgumentException();
                    }

                    if (_lastTimestamp == timestamp)
                    {
                        // 同一时间生成的，则序号+1
                        _sequence = (_sequence + 1) & SequenceMask;
                        // 毫秒内序列溢出：超过最大值
                        if (_sequence == 0)
                        {
                            // 阻塞到下一个毫秒，获得新的时间戳
                            timestamp = GetNextTimestamp(_lastTimestamp);
                        }
                    }
                    else
                    {
                        // 时间戳改变，毫秒内序列重置
                        _sequence = 0L;
                    }

                    // 上次生成 ID 的时间戳
                    _lastTimestamp = timestamp;

                    // 移位并通过或运算拼到一起
                    return ((timestamp - Twepoch) << (int)TimestampLeftShift)
                           | (_dataId << (int)DatacenterIdShift)
                           | (_machineId << (int)MachineIdShift)
                           | _sequence;
                }
            }

            long GetNextTimestamp(long lastTimestamp)
            {
                var timestamp = GetTimestamp();
                while (timestamp <= lastTimestamp)
                {
                    timestamp = GetTimestamp();
                }

                return timestamp;
            }

            public Func<long> GetTimestamp { get; set; }

            //static long GetTimestamp()
            //{
            //    DateTimeOffset dto = DateTime.Now;
            //    return dto.ToUnixTimeMilliseconds();
            //}
        }
    }
}

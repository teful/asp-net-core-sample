﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;

namespace BenchmarkDotNetSample
{
    [SimpleJob(RuntimeMoniker.Net80)]
    [SimpleJob(RuntimeMoniker.Net70, baseline: true)]
    //[SimpleJob(RuntimeMoniker.Net60)]
    [HideColumns("Error", "StdDev", "RatioSD")]
    public class JsonSerializerTest
    {
        //public IEnumerable<WeatherForecast> Source()
        //{
        //    yield return new WeatherForecast()
        //    {
        //        Date = DateTime.UtcNow.AddHours(-1),
        //        TemperatureCelsius = Random.Shared.Next(0, 10000),
        //        Summary = Guid.NewGuid().ToString()
        //    };
        //    yield return new WeatherForecast()
        //    {
        //        Date = DateTime.UtcNow,
        //        TemperatureCelsius = Random.Shared.Next(0, 10000),
        //        Summary = Guid.NewGuid().ToString()
        //    };
        //    yield return new WeatherForecast()
        //    {
        //        Date = DateTime.UtcNow.AddHours(1),
        //        TemperatureCelsius = Random.Shared.Next(0, 10000),
        //        Summary = Guid.NewGuid().ToString()
        //    };
        //}

        public IEnumerable<ReissueMsgLogModel> Source()
        {
            yield return new ReissueMsgLogModel(DateTime.UtcNow, Random.Shared.Next(0, 10000), Guid.NewGuid().ToString());
        }

        private readonly JsonSerializerOptions _options;
        private readonly JsonSerializerOptions _ordinaryOptions;
        public JsonSerializerTest()
        {
            _options = new JsonSerializerOptions
            {
#if NET7_0_OR_GREATER
                TypeInfoResolver = ClassSourceGenerationContext.Default
#endif
            };
            _ordinaryOptions = new JsonSerializerOptions()
            {
                Encoder = JavaScriptEncoder.Create(UnicodeRanges.All)
            };

            ArrayPool<byte> pool = ArrayPool<byte>.Create();
            pool.Rent(10);

            var buff = new byte[1024];
            pool.Return(buff, true);
        }

        [Benchmark]
        [ArgumentsSource(nameof(Source))]
        public byte[] JsonTypeInfoSerializer(ReissueMsgLogModel source)
        {
            return JsonSerializer.SerializeToUtf8Bytes(source, StructSourceGenerationContext.Default.ReissueMsgLogModel);
        }

        [Benchmark]
        [ArgumentsSource(nameof(Source))]
        public byte[] JsonSerializerContextSerializer(ReissueMsgLogModel source)
        {
            return JsonSerializer.SerializeToUtf8Bytes(source, typeof(ReissueMsgLogModel), StructSourceGenerationContext.Default);
        }

        [Benchmark]
        [ArgumentsSource(nameof(Source))]
        public byte[] JsonSerializerOptionsSerializer(ReissueMsgLogModel source)
        {
            return JsonSerializer.SerializeToUtf8Bytes(source, typeof(ReissueMsgLogModel), this._options);
        }

        [Benchmark]
        [ArgumentsSource(nameof(Source))]
        public byte[] OrdinarySerializer(ReissueMsgLogModel source)
        {
            return JsonSerializer.SerializeToUtf8Bytes(source, this._ordinaryOptions);
        }
    }

    [JsonSourceGenerationOptions(WriteIndented = true)]
    [JsonSerializable(typeof(WeatherForecast))]
    internal partial class ClassSourceGenerationContext : JsonSerializerContext
    {
    }

    public class WeatherForecast
    {
        public DateTime Date { get; set; }
        public int TemperatureCelsius { get; set; }
        public string? Summary { get; set; }
    }


    [JsonSourceGenerationOptions(WriteIndented = true)]
    [JsonSerializable(typeof(ReissueMsgLogModel))]
    internal partial class StructSourceGenerationContext : JsonSerializerContext
    {
    }
    public readonly record struct ReissueMsgLogModel
    {
        public DateTime Date { get; }
        public int TemperatureCelsius { get; }
        public string? Summary { get; }

        [JsonConstructor]
        public ReissueMsgLogModel(DateTime date, int temperatureCelsius, string summary)
        {
            Date = date;
            TemperatureCelsius = temperatureCelsius;
            Summary = summary;
        }
    }
}

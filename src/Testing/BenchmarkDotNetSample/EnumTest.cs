﻿using BenchmarkDotNet.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace BenchmarkDotNetSample
{
    /// <summary>
    /// 枚举转字符串
    /// </summary>
    [MemoryDiagnoser]
    public class EnumToStringTest
    {
        [Benchmark]
        public string NativeToString()
        {
            HumanStates state = HumanStates.Sleeping;
            return state.ToString();
        }

        [Benchmark]
        public string SyntaxToString()
        {
            return $"{HumanStates.Sleeping}";
        }

        [Benchmark]
        public string SwitchStatementToString()
        {
            HumanStates state = HumanStates.Sleeping;
            return SwitchStatementToString(state);
        }

        [Benchmark(Baseline = true)]
        public string SwitchExpressionToString()
        {
            HumanStates state = HumanStates.Sleeping;
            return SwitchExpressionToString(state);
        }

        public string SwitchStatementToString(HumanStates state)
        {
            switch (state)
            {
                case HumanStates.Idle:
                    return nameof(HumanStates.Idle);
                case HumanStates.Working:
                    return nameof(HumanStates.Working);
                case HumanStates.Sleeping:
                    return nameof(HumanStates.Sleeping);
                case HumanStates.Eating:
                    return nameof(HumanStates.Eating);
                case HumanStates.Dead:
                    return nameof(HumanStates.Dead);
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }

        public string SwitchExpressionToString(HumanStates state)
        {
            return state switch
            {
                HumanStates.Idle => nameof(HumanStates.Idle),
                HumanStates.Working => nameof(HumanStates.Working),
                HumanStates.Sleeping => nameof(HumanStates.Sleeping),
                HumanStates.Eating => nameof(HumanStates.Eating),
                HumanStates.Dead => nameof(HumanStates.Dead),
                _ => throw new ArgumentOutOfRangeException(nameof(state), state, null)
            };
        }
    }

    /// <summary>
    /// 枚举转描述
    /// </summary>
    [MemoryDiagnoser]
    public class EnumDescTest
    {
        private readonly IDictionary<Enum, string> _stateDescMap;
        public EnumDescTest()
        {
            _stateDescMap = new Dictionary<Enum, string>();
        }

        [Benchmark]
        public string GetNotCachingDesc()
        {
            HumanStates state = HumanStates.Idle;
            return GetDescription(state);
        }

        [Benchmark(Baseline = true)]
        public string GetCachingDesc()
        {
            HumanStates state = HumanStates.Idle;
            if (this._stateDescMap.TryGetValue(state, out var desc))
            {
                return desc;
            }

            desc = GetDescription(HumanStates.Idle);
            this._stateDescMap[state] = desc;
            return desc;
        }

        public string GetDescription(Enum value)
        {
            var type = value.GetType();
            var name = Enum.GetName(type, value);
            if (string.IsNullOrWhiteSpace(name))
            {
                return value.ToString();
            }

            var field = type.GetField(name);
            var des = field?.GetCustomAttribute<DescriptionAttribute>(); // using System.Reflection;
            if (des == default)
            {
                return value.ToString();
            }

            return des.Description;
        }
    }

    /// <summary>
    /// 单个枚举值比较
    /// </summary>
    [MemoryDiagnoser]
    public class SingleEnumCompareTest
    {
        private readonly HumanStates _defaultState = HumanStates.Working;

        [Benchmark]
        public bool NativeEquals()
        {
            HumanStates state = HumanStates.Eating;
            return this._defaultState.Equals(state);
        }

        [Benchmark(Baseline = true)]
        public bool OperatorEquals()
        {
            HumanStates state = HumanStates.Eating;
            return this._defaultState == state;
        }

        #region Flags

        [Benchmark]
        public bool FlagEquals()
        {
            HumanStates state = HumanStates.Eating;
            return this._defaultState.HasFlag(state);
        }

        [Benchmark]
        public bool BitwiseEquals()
        {
            HumanStates state = HumanStates.Eating;
            return (state & this._defaultState) > 0;
        }

        #endregion

    }

    /// <summary>
    /// 多个枚举值比较
    /// </summary>
    [MemoryDiagnoser]
    public class MultiEnumCompareTest
    {
        private readonly HumanStates _defaultState = HumanStates.Working | HumanStates.Sleeping;

        #region Flags

        [Benchmark]
        public bool FlagEquals()
        {
            HumanStates state = HumanStates.Eating;
            return this._defaultState.HasFlag(state);
        }

        [Benchmark(Baseline = true)]
        public bool BitwiseEquals()
        {
            HumanStates state = HumanStates.Eating;
            return (state & this._defaultState) == state;
        }

        [Benchmark]
        public bool Bitwise1Equals()
        {
            HumanStates state = HumanStates.Eating;
            return (state & this._defaultState) > 0;
        }

        #endregion

    }

    [Flags]
    public enum HumanStates : byte
    {
        [Description("Not doing anything")]
        Idle = 1,// 1 << 0
        Working = 1 << 1,// 2
        Sleeping = 4,// 1 << 2
        Eating = 1 << 3,// 8
        Dead = 16 // 1 << 4
    }
}

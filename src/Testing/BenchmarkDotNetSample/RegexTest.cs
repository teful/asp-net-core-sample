﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace BenchmarkDotNetSample
{
    /// <summary>
    /// 微软正则性能测试代码
    /// dotnet run -c release -f net5.0
    /// </summary>
    [SimpleJob(RuntimeMoniker.Net80)]
    [SimpleJob(RuntimeMoniker.Net70)]
    [SimpleJob(RuntimeMoniker.Net60)]
    [SimpleJob(RuntimeMoniker.Net50)]
    public class RegexTest
    {
        private Regex _regex = new Regex("[a-zA-Z0-9]*", RegexOptions.Compiled);

        [Benchmark]
        public bool IsMatch() => _regex.IsMatch("abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ");

        [Benchmark]
        [Arguments("abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")]
        [Arguments("ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789abcdefghijklmnopqrstuvwxyz")]
        [ArgumentsSource(nameof(GetSources))]
        public bool IsMatch(string source) => _regex.IsMatch(source);

        public IEnumerable<string> GetSources()
        {
            yield return "abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            yield return "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789abcdefghijklmnopqrstuvwxyz";
        }
    }
}

﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BenchmarkDotNetSample
{
    [SimpleJob(RuntimeMoniker.Net80)]
    [SimpleJob(RuntimeMoniker.Net70)]
    //[SimpleJob(RuntimeMoniker.Net60)]
    [HideColumns("Error", "StdDev", "RatioSD")]
    public class KeywordTest
    {
        const string SINGLE_SOURCE_PATH = @"C:\Users\dev122\Desktop\keywords.txt";
        const string COMBIN_SOURCE_PATH = @"C:\Users\dev122\Desktop\combin_keywords.txt";


        private readonly SingleMatcher _singleMatcher;
        //private readonly CombinMatcher _combinMatcher;
        private readonly SingleMatcherV2 _singleMatcherV2;
        private readonly SingleMatcherV3 _singleMatcherV3;
        private readonly CombinMatcherV2 _combinMatcherV2;
        //private readonly CombinMatcherV3 _combinMatcherV3;

        public KeywordTest()
        {
            var keywords = File.ReadAllLines(SINGLE_SOURCE_PATH);
            _singleMatcher = new SingleMatcher(keywords);

            var combinKeywords = File.ReadAllLines(COMBIN_SOURCE_PATH);
            //_combinMatcher = new CombinMatcher(combinKeywords);

            _singleMatcherV2 = new SingleMatcherV2(keywords);
            _singleMatcherV3 = new SingleMatcherV3(keywords);

            _combinMatcherV2 = new CombinMatcherV2(combinKeywords);
            //_combinMatcherV3 = new CombinMatcherV3(combinKeywords);
        }

        public IEnumerable<string> Source()
        {
            yield return "【蒙策咨询】辅佐本地餐饮商入驻抖音外卖，1~3个工作日即可开通成功 回复1了解详情，拒收请回复R";
        }

        [Benchmark(Baseline = true, Description = "单个词匹配")]
        [ArgumentsSource(nameof(Source))]
        public int Match(string source)
        {
            var ressult = this._singleMatcher.Searching(source);
            return ressult.Count();
        }

        [Benchmark(Description = "单个词匹配V2")]
        [ArgumentsSource(nameof(Source))]
        public int MatchV2(string source)
        {
            var ressult = this._singleMatcherV2.Searching(source);
            return ressult.Count();
        }

        [Benchmark(Description = "单个词匹配V3")]
        [ArgumentsSource(nameof(Source))]
        public int MatchV3(string source)
        {
            var ressult = this._singleMatcherV3.Searching(source);
            return ressult.Count();
        }

        //[Benchmark(Description = "组合词匹配")]
        //[ArgumentsSource(nameof(Source))]
        //public int CombinMatch(string source)
        //{
        //    var ressult = this._combinMatcher.Searching(source);
        //    return ressult.Count();
        //}

        [Benchmark(Description = "组合词匹配正则")]
        [ArgumentsSource(nameof(Source))]
        public int CombinMatchV2(string source)
        {
            var ressult = this._combinMatcherV2.Searching(source);
            return ressult.Count();
        }

        //[Benchmark(Description = "组合词匹配V3")]
        //[ArgumentsSource(nameof(Source))]
        //public int CombinMatchV3(string source)
        //{
        //    var ressult = this._combinMatcherV3.Searching(source);
        //    return ressult.Count();
        //}

        #region Single

        public sealed class SingleMatcher
        {
            private readonly ACMatcher _matcher;
            public SingleMatcher(IEnumerable<string> keywords)
            {
                Trie trie = new();
                foreach (var element in keywords)
                {
                    trie.Insert(element);
                }
                var automaton = new ACAutomaton(trie.Root);

                _matcher = new ACMatcher(automaton);
            }

            public IEnumerable<string> Searching(string source)
            {
                return this._matcher.FindKeywords(source);
            }

            public class ACMatcher
            {
                private ACAutomaton automaton;

                public ACMatcher(ACAutomaton automaton)
                {
                    this.automaton = automaton;
                }

                public List<string> FindKeywords(string text)
                {
                    List<string> keywords = new List<string>();
                    TrieNode? current = automaton.Root;

                    if (current == null)
                    {
                        return keywords;
                    }

                    for (int i = 0; i < text.Length; i++)
                    {
                        char c = text[i];

                        // 在失败指针链上查找匹配的子节点
                        while (current != automaton.Root && !current!.Children.ContainsKey(c))
                        {
                            current = current.Failure;
                        }

                        if (current.Children.ContainsKey(c))
                        {
                            current = current.Children[c];
                        }

                        // 如果当前节点是关键词的结尾，则添加关键词到结果列表
                        if (current.IsEndOfWord)
                        {
                            string keyword = text.Substring(i - current.Depth + 1, current.Depth);
                            keywords.Add(keyword);
                        }
                    }

                    return keywords;
                }

                public List<(string, (int, int))> FindCombinKeywords(string text)
                {
                    List<(string, (int, int))> keywords = new();
                    TrieNode? current = automaton.Root;

                    if (current == null)
                    {
                        return keywords;
                    }

                    for (int i = 0; i < text.Length; i++)
                    {
                        char c = text[i];

                        // 在失败指针链上查找匹配的子节点
                        while (current != automaton.Root && !current!.Children.ContainsKey(c))
                        {
                            current = current.Failure;
                        }

                        if (current.Children.ContainsKey(c))
                        {
                            current = current.Children[c];
                        }

                        // 如果当前节点是关键词的结尾，则添加关键词到结果列表
                        if (current.IsEndOfWord)
                        {
                            string keyword = text.Substring(i - current.Depth + 1, current.Depth);
                            keywords.Add((keyword, (i - current.Depth + 1, i - current.Depth + 1 + current.Depth)));
                        }
                    }

                    return keywords;
                }
            }

            public class ACAutomaton
            {
                private TrieNode _root;

                public TrieNode Root => _root;

                public ACAutomaton(TrieNode root)
                {
                    this._root = root;
                    BuildFailurePointers();
                }

                private void BuildFailurePointers()
                {
                    Queue<TrieNode> queue = new Queue<TrieNode>();

                    // 设置根节点的失败指针为自身
                    _root.Failure = _root;

                    // 将根节点的子节点加入队列，并设置其失败指针为根节点
                    foreach (TrieNode child in _root.Children.Values)
                    {
                        child.Failure = _root;
                        queue.Enqueue(child);
                    }

                    // 通过BFS遍历每个节点，为其设置失败指针
                    while (queue.Count > 0)
                    {
                        TrieNode current = queue.Dequeue();

                        foreach (TrieNode child in current.Children.Values)
                        {
                            char c = child.Value;
                            TrieNode? failure = current.Failure;

                            // 在失败指针链上查找匹配的子节点
                            while (failure != null && failure != _root && !failure.Children.ContainsKey(c))
                            {
                                failure = failure.Failure;
                            }

                            if (failure != null && failure.Children.ContainsKey(c))
                            {
                                child.Failure = failure.Children[c];
                            }
                            else
                            {
                                child.Failure = _root;
                            }

                            queue.Enqueue(child);
                        }
                    }
                }
            }

            public class TrieNode
            {
                public char Value { get; set; }
                public bool IsEndOfWord { get; set; }
                public Dictionary<char, TrieNode> Children { get; set; }
                public TrieNode? Failure { get; internal set; }
                public int Depth { get; internal set; }

                public TrieNode(char value)
                {
                    Value = value;
                    IsEndOfWord = false;
                    Children = new Dictionary<char, TrieNode>();
                    Depth = 0;
                }
            }

            public class Trie
            {
                private TrieNode _root;

                public TrieNode Root => this._root;

                public Trie()
                {
                    _root = new TrieNode('\0');
                }

                public void Insert(string word)
                {
                    TrieNode current = _root;
                    foreach (char c in word)
                    {
                        if (!current.Children.ContainsKey(c))
                        {
                            current.Children[c] = new TrieNode(c);
                        }
                        current = current.Children[c];
                    }
                    current.IsEndOfWord = true;
                    CalculateDepth(_root, 0);
                }

                public void CalculateDepth(TrieNode node, int depth)
                {
                    foreach (var child in node.Children.Values)
                    {
                        child.Depth = depth + 1;
                        CalculateDepth(child, depth + 1);
                    }
                }
            }

        }
        #endregion

        #region Single V2

        class SingleMatcherV2
        {
            protected ushort[] _dict;
            protected int[] _first;

            protected IntDictionary[] _nextIndex;
            protected int[] _end;
            protected int[] _resultIndex;
            protected int[] _keywordLengths;

            public SingleMatcherV2(IEnumerable<string> keywords)
            {
                this.SetKeywords(keywords.ToArray());
            }

            #region 设置关键字
            /// <summary>
            /// 设置关键字
            /// </summary>
            /// <param name="keywords">关键字列表</param>
            public virtual void SetKeywords(ICollection<string> keywords)
            {
                _keywordLengths = new int[keywords.Count];
                int index = 0;
                foreach (var item in keywords)
                {
                    _keywordLengths[index++] = item.Length;
                }

                SetKeywords2(keywords.ToArray());
            }
            private void SetKeywords2(ICollection<string> _keywords)
            {
                var root = new TrieNode();
                Dictionary<int, List<TrieNode>> allNodeLayers = new Dictionary<int, List<TrieNode>>();
                int kindex = 0;
                foreach (string p in _keywords)
                {
                    var nd = root;
                    for (int j = 0; j < p.Length; j++)
                    {
                        nd = nd.Add((char)p[j]);
                        if (nd.Layer == 0)
                        {
                            nd.Layer = j + 1;
                            List<TrieNode> trieNodes;
                            if (allNodeLayers.TryGetValue(nd.Layer, out trieNodes) == false)
                            {
                                trieNodes = new List<TrieNode>();
                                allNodeLayers[nd.Layer] = trieNodes;
                            }
                            trieNodes.Add(nd);
                        }
                    }
                    nd.SetResults(kindex++);
                }

                List<TrieNode> allNode = new List<TrieNode>();
                allNode.Add(root);
                foreach (var trieNodes in allNodeLayers)
                {
                    foreach (var nd in trieNodes.Value)
                    {
                        allNode.Add(nd);
                    }
                }
                allNodeLayers = null;


                for (int i = 1; i < allNode.Count; i++)
                {
                    var nd = allNode[i];
                    nd.Index = i;
                    TrieNode r = nd.Parent.Failure;
                    char c = nd.Char;
                    while (r != null && (r.m_values == null || !r.m_values.ContainsKey(c))) r = r.Failure;
                    if (r == null)
                        nd.Failure = root;
                    else
                    {
                        nd.Failure = r.m_values[c];
                        if (nd.Failure.Results != null)
                        {
                            foreach (var result in nd.Failure.Results)
                                nd.SetResults(result);
                        }
                    }
                }
                root.Failure = root;

                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 1; i < allNode.Count; i++)
                {
                    stringBuilder.Append(allNode[i].Char);
                }
                var length = CreateDict(stringBuilder.ToString());
                stringBuilder = null;

                var first = new int[Char.MaxValue + 1];
                if (allNode[0].m_values != null)
                {
                    foreach (var item in allNode[0].m_values)
                    {
                        var key = (char)_dict[item.Key];
                        first[key] = item.Value.Index;
                    }
                }
                _first = first;

                var resultIndex2 = new List<int>();
                var isEndStart = new List<bool>();
                var _nextIndex2 = new IntDictionary[allNode.Count];

                for (int i = allNode.Count - 1; i >= 0; i--)
                {
                    var dict = new Dictionary<ushort, int>();
                    var result = new List<int>();
                    var oldNode = allNode[i];

                    if (oldNode.m_values != null)
                    {
                        foreach (var item in oldNode.m_values)
                        {
                            var key = (char)_dict[item.Key];
                            var index = item.Value.Index;
                            dict[key] = index;
                        }
                    }
                    if (oldNode.Results != null)
                    {
                        foreach (var item in oldNode.Results)
                        {
                            if (result.Contains(item) == false)
                            {
                                result.Add(item);
                            }
                        }
                    }

                    oldNode = oldNode.Failure;
                    while (oldNode != root)
                    {
                        if (oldNode.m_values != null)
                        {
                            foreach (var item in oldNode.m_values)
                            {
                                var key = (char)_dict[item.Key];
                                var index = item.Value.Index;
                                if (dict.ContainsKey(key) == false)
                                {
                                    dict[key] = index;
                                }
                            }
                        }
                        if (oldNode.Results != null)
                        {
                            foreach (var item in oldNode.Results)
                            {
                                if (result.Contains(item) == false)
                                {
                                    result.Add(item);
                                }
                            }
                        }
                        oldNode = oldNode.Failure;
                    }
                    _nextIndex2[i] = new IntDictionary(dict);

                    if (result.Count > 0)
                    {
                        for (int j = result.Count - 1; j >= 0; j--)
                        {
                            resultIndex2.Add(result[j]);
                            isEndStart.Add(false);
                        }
                        isEndStart[isEndStart.Count - 1] = true;
                    }
                    else
                    {
                        resultIndex2.Add(-1);
                        isEndStart.Add(true);
                    }
                    dict = null;
                    result = null;
                    allNode[i].Dispose();
                    allNode.RemoveAt(i);
                }
                allNode.Clear();
                allNode = null;
                root = null;
                _nextIndex = _nextIndex2;

                var resultIndex = new List<int>();
                var end = new List<int>() { };
                for (int i = isEndStart.Count - 1; i >= 0; i--)
                {
                    if (isEndStart[i])
                    {
                        end.Add(resultIndex.Count);
                    }
                    if (resultIndex2[i] > -1)
                    {
                        resultIndex.Add(resultIndex2[i]);
                    }
                }
                end.Add(resultIndex.Count);
                _resultIndex = resultIndex.ToArray();
                _end = end.ToArray();
            }

            #endregion

            #region 生成映射字典

            private int CreateDict(string keywords)
            {
                Dictionary<char, Int32> dictionary = new Dictionary<char, Int32>();
                foreach (var item in keywords)
                {
                    if (dictionary.ContainsKey(item))
                    {
                        dictionary[item] += 1;
                    }
                    else
                    {
                        dictionary[item] = 1;
                    }
                }
                var list = dictionary.OrderByDescending(q => q.Value).Select(q => q.Key).ToList();
                var list2 = new List<char>();
                var sh = false;
                foreach (var item in list)
                {
                    if (sh)
                    {
                        list2.Add(item);
                    }
                    else
                    {
                        list2.Insert(0, item);
                    }
                    sh = !sh;
                }
                _dict = new ushort[char.MaxValue + 1];
                for (Int32 i = 0; i < list2.Count; i++)
                {
                    _dict[list2[i]] = (ushort)(i + 1);
                }
                return dictionary.Count;
            }

            #endregion

            public IEnumerable<string> Searching(string source)
            {
                List<string> result = new List<string>();
                var p = 0;
                var txt = source.AsSpan();
                for (int i = 0; i < txt.Length; i++)
                {
                    var t = _dict[txt[i]];

                    if (t == 0)
                    {
                        p = 0;
                        continue;
                    }

                    if (p == 0 || _nextIndex[p].TryGetValue(t, out int next) == false)
                    {
                        next = _first[t];
                    }

                    if (next != 0)
                    {
                        for (int j = _end[next]; j < _end[next + 1]; j++)
                        {
                            var index = _resultIndex[j];
                            var len = _keywordLengths[index];
                            var key = txt.Slice(i + 1 - len, len).ToString();
                            result.Add(key);
                        }
                    }

                    p = next;
                }
                return result;
            }

            sealed class TrieNode //:IDisposable
            {
                public int Index;
                public int Layer;
                public bool End { get { return Results != null; } }
                public char Char;
                public List<int> Results;
                public Dictionary<char, TrieNode> m_values;
                public TrieNode Failure;
                public TrieNode Parent;
                public bool IsWildcard;
                public int WildcardLayer;
                public bool HasWildcard;


                public TrieNode Add(char c)
                {
                    TrieNode node;
                    if (m_values == null)
                    {
                        m_values = new Dictionary<char, TrieNode>();
                    }
                    else if (m_values.TryGetValue(c, out node))
                    {
                        return node;
                    }
                    node = new TrieNode();
                    node.Parent = this;
                    node.Char = c;
                    m_values[c] = node;
                    return node;
                }

                public void SetResults(int index)
                {

                    if (Results == null)
                    {
                        Results = new List<int>();
                    }
                    Results.Add(index);
                }
                /// <summary>
                /// 伪释放
                /// </summary>
                public void Dispose()
                {
                    if (Results != null)
                    {
                        Results.Clear();
                        Results = null;
                    }
                    if (m_values != null)
                    {
                        m_values.Clear();
                        m_values = null;
                    }
                    Failure = null;
                    Parent = null;
                }
            }

            public struct IntDictionary
            {
                private ushort[] _keys;
                private int[] _values;
                private int last;
                public IntDictionary(ushort[] keys, int[] values)
                {
                    _keys = keys;
                    _values = values;
                    last = keys.Length - 1;
                }
                public IntDictionary(Dictionary<ushort, int> dict)
                {
                    var keys = dict.Select(q => q.Key).OrderBy(q => q).ToArray();
                    var values = new int[keys.Length];
                    for (int i = 0; i < keys.Length; i++)
                    {
                        values[i] = dict[keys[i]];
                    }
                    _keys = keys;
                    _values = values;
                    last = keys.Length - 1;
                }


                public ushort[] Keys
                {
                    get
                    {
                        return _keys;
                    }
                }

                public int[] Values
                {
                    get
                    {
                        return _values;
                    }
                }

                public bool TryGetValue(ushort key, out int value)
                {
                    if (last == -1)
                    {
                        value = 0;
                        return false;
                    }
                    if (_keys[0] == key)
                    {
                        value = _values[0];
                        return true;
                    }
                    else if (last == 0 || _keys[0] > key)
                    {
                        value = 0;
                        return false;
                    }

                    if (_keys[last] == key)
                    {
                        value = _values[last];
                        return true;
                    }
                    else if (_keys[last] < key)
                    {
                        value = 0;
                        return false;
                    }

                    var left = 1;
                    var right = last - 1;
                    while (left <= right)
                    {
                        int mid = (left + right) >> 1;
                        int d = _keys[mid] - key;

                        if (d == 0)
                        {
                            value = _values[mid];
                            return true;
                        }
                        else if (d > 0)
                        {
                            right = mid - 1;
                        }
                        else
                        {
                            left = mid + 1;
                        }
                    }
                    value = 0;
                    return false;
                }
            }
        }

        #endregion

        #region Single V3

        public class SingleMatcherV3
        {
            private readonly WordsSearchEx _wordsSearch;

            public SingleMatcherV3(IEnumerable<string> keywords)
            {
                _wordsSearch = new WordsSearchEx();
                _wordsSearch.SetKeywords(keywords.ToArray());
            }

            public IEnumerable<string> Searching(string source)
            {
                return _wordsSearch.FindAll(source).Select(r => r.MatchKeyword);
            }

            public sealed class WordsSearchEx : BaseSearchEx
            {
                /// <summary>
                /// 在文本中查找所有的关键字
                /// </summary>
                /// <param name="text">文本</param>
                /// <returns></returns>
                public List<WordsSearchResult> FindAll(string text)
                {
                    List<WordsSearchResult> result = new List<WordsSearchResult>();
                    var p = 0;
                    var txt = text.AsSpan();
                    for (int i = 0; i < txt.Length; i++)
                    {
                        var t = _dict[txt[i]];
                        if (t == 0)
                        {
                            p = 0;
                            continue;
                        }
                        int next;
                        if (p == 0 || _nextIndex[p].TryGetValue(t, out next) == false)
                        {
                            next = _first[t];
                        }
                        if (next != 0)
                        {
                            for (int j = _end[next]; j < _end[next + 1]; j++)
                            {
                                var index = _resultIndex[j];
                                var len = _keywordLengths[index];
                                var st = i + 1 - len;
                                var r = new WordsSearchResult(ref text, st, i, index);
                                result.Add(r);
                            }
                        }
                        p = next;
                    }
                    return result;
                }
                /// <summary>
                /// 在文本中查找所有的关键字
                /// </summary>
                /// <param name="text">文本</param>
                /// <returns></returns>
                public List<WordsSearchResult> FindAll2(string text)
                {
                    List<WordsSearchResult> result = new List<WordsSearchResult>();
                    var p = 0;
                    for (int i = 0; i < text.Length; i++)
                    {
                        var t = _dict[text[i]];
                        if (t == 0)
                        {
                            p = 0;
                            continue;
                        }
                        int next;
                        if (p == 0 || _nextIndex[p].TryGetValue(t, out next) == false)
                        {
                            next = _first[t];
                        }
                        if (next != 0)
                        {
                            for (int j = _end[next]; j < _end[next + 1]; j++)
                            {
                                var index = _resultIndex[j];
                                var len = _keywordLengths[index];
                                var st = i + 1 - len;
                                var r = new WordsSearchResult(ref text, st, i, index);
                                result.Add(r);
                            }
                        }
                        p = next;
                    }
                    return result;
                }
            }
            public class WordsSearchResult
            {
                public WordsSearchResult(string keyword, int start, int end, int index)
                {
                    _keyword = keyword;
                    End = end;
                    Start = start;
                    Index = index;
                    _matchKeyword = keyword;
                }

                public WordsSearchResult(ref string text, int start, int end, int index)
                {
                    _text = text;
                    End = end;
                    Start = start;
                    Index = index;
                }


                public WordsSearchResult(string keyword, int start, int end, int index, string matchKeyword)
                {
                    _keyword = keyword;
                    End = end;
                    Start = start;
                    Index = index;
                    _matchKeyword = matchKeyword;
                }
                private string _text;
                private string _keyword;
                private string _matchKeyword;

                /// <summary>
                /// 开始位置
                /// </summary>
                public int Start { get; private set; }

                /// <summary>
                /// 结束位置
                /// </summary>
                public int End { get; private set; }


                /// <summary>
                /// 关键字
                /// </summary>
                public string Keyword
                {
                    get
                    {
                        if (_keyword == null)
                        {
                            _keyword = _text.Substring(Start, End + 1 - Start);
                        }
                        return _keyword;
                    }
                }

                /// <summary>
                /// 索引
                /// </summary>
                public int Index { get; private set; }

                /// <summary>
                /// 匹配关键字
                /// </summary>
                public string MatchKeyword
                {
                    get
                    {
                        if (_matchKeyword == null)
                        {
                            if (_keyword == null)
                            {
                                _matchKeyword = _text.Substring(Start, End + 1 - Start);
                            }
                            else
                            {
                                _matchKeyword = _keyword;
                            }
                        }
                        return _matchKeyword;
                    }
                }


                public override string ToString()
                {
                    if (MatchKeyword != Keyword)
                    {
                        return Start.ToString() + "|" + Keyword + "|" + MatchKeyword;
                    }
                    return Start.ToString() + "|" + Keyword;
                }
            }
            public abstract class BaseSearchEx
            {
                protected ushort[] _dict;
                protected int[] _first;

                protected IntDictionary[] _nextIndex;
                protected int[] _end;
                protected int[] _resultIndex;
                protected int[] _keywordLengths;


                #region 设置关键字
                /// <summary>
                /// 设置关键字
                /// </summary>
                /// <param name="keywords">关键字列表</param>
                public virtual void SetKeywords(ICollection<string> keywords)
                {
                    _keywordLengths = new int[keywords.Count];
                    int index = 0;
                    foreach (var item in keywords)
                    {
                        _keywordLengths[index++] = item.Length;
                    }

                    SetKeywords2(keywords.ToArray());
                }
                private void SetKeywords2(ICollection<string> _keywords)
                {
                    var root = new TrieNode();
                    Dictionary<int, List<TrieNode>> allNodeLayers = new Dictionary<int, List<TrieNode>>();
                    int kindex = 0;
                    foreach (string p in _keywords)
                    {
                        var nd = root;
                        for (int j = 0; j < p.Length; j++)
                        {
                            nd = nd.Add((char)p[j]);
                            if (nd.Layer == 0)
                            {
                                nd.Layer = j + 1;
                                List<TrieNode> trieNodes;
                                if (allNodeLayers.TryGetValue(nd.Layer, out trieNodes) == false)
                                {
                                    trieNodes = new List<TrieNode>();
                                    allNodeLayers[nd.Layer] = trieNodes;
                                }
                                trieNodes.Add(nd);
                            }
                        }
                        nd.SetResults(kindex++);
                    }

                    List<TrieNode> allNode = new List<TrieNode>();
                    allNode.Add(root);
                    foreach (var trieNodes in allNodeLayers)
                    {
                        foreach (var nd in trieNodes.Value)
                        {
                            allNode.Add(nd);
                        }
                    }
                    allNodeLayers = null;


                    for (int i = 1; i < allNode.Count; i++)
                    {
                        var nd = allNode[i];
                        nd.Index = i;
                        TrieNode r = nd.Parent.Failure;
                        char c = nd.Char;
                        while (r != null && (r.m_values == null || !r.m_values.ContainsKey(c))) r = r.Failure;
                        if (r == null)
                            nd.Failure = root;
                        else
                        {
                            nd.Failure = r.m_values[c];
                            if (nd.Failure.Results != null)
                            {
                                foreach (var result in nd.Failure.Results)
                                    nd.SetResults(result);
                            }
                        }
                    }
                    root.Failure = root;

                    StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 1; i < allNode.Count; i++)
                    {
                        stringBuilder.Append(allNode[i].Char);
                    }
                    var length = CreateDict(stringBuilder.ToString());
                    stringBuilder = null;

                    var first = new int[Char.MaxValue + 1];
                    if (allNode[0].m_values != null)
                    {
                        foreach (var item in allNode[0].m_values)
                        {
                            var key = (char)_dict[item.Key];
                            first[key] = item.Value.Index;
                        }
                    }
                    _first = first;

                    var resultIndex2 = new List<int>();
                    var isEndStart = new List<bool>();
                    var _nextIndex2 = new IntDictionary[allNode.Count];

                    for (int i = allNode.Count - 1; i >= 0; i--)
                    {
                        var dict = new Dictionary<ushort, int>();
                        var result = new List<int>();
                        var oldNode = allNode[i];

                        if (oldNode.m_values != null)
                        {
                            foreach (var item in oldNode.m_values)
                            {
                                var key = (char)_dict[item.Key];
                                var index = item.Value.Index;
                                dict[key] = index;
                            }
                        }
                        if (oldNode.Results != null)
                        {
                            foreach (var item in oldNode.Results)
                            {
                                if (result.Contains(item) == false)
                                {
                                    result.Add(item);
                                }
                            }
                        }

                        oldNode = oldNode.Failure;
                        while (oldNode != root)
                        {
                            if (oldNode.m_values != null)
                            {
                                foreach (var item in oldNode.m_values)
                                {
                                    var key = (char)_dict[item.Key];
                                    var index = item.Value.Index;
                                    if (dict.ContainsKey(key) == false)
                                    {
                                        dict[key] = index;
                                    }
                                }
                            }
                            if (oldNode.Results != null)
                            {
                                foreach (var item in oldNode.Results)
                                {
                                    if (result.Contains(item) == false)
                                    {
                                        result.Add(item);
                                    }
                                }
                            }
                            oldNode = oldNode.Failure;
                        }
                        _nextIndex2[i] = new IntDictionary(dict);

                        if (result.Count > 0)
                        {
                            for (int j = result.Count - 1; j >= 0; j--)
                            {
                                resultIndex2.Add(result[j]);
                                isEndStart.Add(false);
                            }
                            isEndStart[isEndStart.Count - 1] = true;
                        }
                        else
                        {
                            resultIndex2.Add(-1);
                            isEndStart.Add(true);
                        }
                        dict = null;
                        result = null;
                        allNode[i].Dispose();
                        allNode.RemoveAt(i);
                    }
                    allNode.Clear();
                    allNode = null;
                    root = null;
                    _nextIndex = _nextIndex2;

                    var resultIndex = new List<int>();
                    var end = new List<int>() { };
                    for (int i = isEndStart.Count - 1; i >= 0; i--)
                    {
                        if (isEndStart[i])
                        {
                            end.Add(resultIndex.Count);
                        }
                        if (resultIndex2[i] > -1)
                        {
                            resultIndex.Add(resultIndex2[i]);
                        }
                    }
                    end.Add(resultIndex.Count);
                    _resultIndex = resultIndex.ToArray();
                    _end = end.ToArray();
                }

                #endregion

                #region 生成映射字典

                private int CreateDict(string keywords)
                {
                    Dictionary<char, Int32> dictionary = new Dictionary<char, Int32>();
                    foreach (var item in keywords)
                    {
                        if (dictionary.ContainsKey(item))
                        {
                            dictionary[item] += 1;
                        }
                        else
                        {
                            dictionary[item] = 1;
                        }
                    }
                    var list = dictionary.OrderByDescending(q => q.Value).Select(q => q.Key).ToList();
                    var list2 = new List<char>();
                    var sh = false;
                    foreach (var item in list)
                    {
                        if (sh)
                        {
                            list2.Add(item);
                        }
                        else
                        {
                            list2.Insert(0, item);
                        }
                        sh = !sh;
                    }
                    _dict = new ushort[char.MaxValue + 1];
                    for (Int32 i = 0; i < list2.Count; i++)
                    {
                        _dict[list2[i]] = (ushort)(i + 1);
                    }
                    return dictionary.Count;
                }

                #endregion

                #region 保存到文件
                /// <summary>
                /// 保存到文件
                /// </summary>
                /// <param name="filePath"></param>
                public void Save(string filePath)
                {
                    var fs = File.Open(filePath, FileMode.Create);
                    BinaryWriter bw = new BinaryWriter(fs);
                    Save(bw);

                    bw.Close();
                    fs.Close();
                }

                /// <summary>
                /// 保存到Stream
                /// </summary>
                /// <param name="stream"></param>
                public void Save(Stream stream)
                {
                    BinaryWriter bw = new BinaryWriter(stream);
                    Save(bw);
                    bw.Close();
                }

                protected internal virtual void Save(BinaryWriter bw)
                {
                    var bs = IntArrToByteArr(_keywordLengths);
                    bw.Write(bs.Length);
                    bw.Write(bs);

                    bs = IntArrToByteArr(_dict);
                    bw.Write(bs.Length);
                    bw.Write(bs);

                    bs = IntArrToByteArr(_first);
                    bw.Write(bs.Length);
                    bw.Write(bs);

                    bs = IntArrToByteArr(_end);
                    bw.Write(bs.Length);
                    bw.Write(bs);

                    bs = IntArrToByteArr(_resultIndex);
                    bw.Write(bs.Length);
                    bw.Write(bs);

                    bw.Write(_nextIndex.Length);
                    foreach (var dict in _nextIndex)
                    {
                        var keys = dict.Keys;
                        var values = dict.Values;

                        bs = IntArrToByteArr(keys);
                        bw.Write(bs.Length);
                        bw.Write(bs);

                        bs = IntArrToByteArr(values);
                        bw.Write(bs.Length);
                        bw.Write(bs);
                    }
                }

                protected byte[] IntArrToByteArr(Int32[] intArr)
                {
                    Int32 intSize = sizeof(Int32) * intArr.Length;
                    byte[] bytArr = new byte[intSize];
                    Buffer.BlockCopy(intArr, 0, bytArr, 0, intSize);
                    return bytArr;
                }
                protected byte[] IntArrToByteArr(ushort[] intArr)
                {
                    Int32 intSize = sizeof(ushort) * intArr.Length;
                    byte[] bytArr = new byte[intSize];
                    Buffer.BlockCopy(intArr, 0, bytArr, 0, intSize);
                    return bytArr;
                }

                #endregion

                #region 加载文件
                /// <summary>
                /// 加载文件，注意：不是加载原文件，是加载Save后文件
                /// </summary>
                /// <param name="filePath"></param>
                public void Load(string filePath)
                {
                    var fs = File.OpenRead(filePath);
                    BinaryReader br = new BinaryReader(fs);
                    Load(br);
                    br.Close();
                    fs.Close();
                }
                /// <summary>
                /// 加载Stream，注意：不是加载原文件，是加载Save后文件
                /// </summary>
                /// <param name="stream"></param>
                public void Load(Stream stream)
                {
                    BinaryReader br = new BinaryReader(stream);
                    Load(br);
                    br.Close();
                }

                protected internal virtual void Load(BinaryReader br)
                {
                    var length = br.ReadInt32();
                    var bs = br.ReadBytes(length);
                    _keywordLengths = ByteArrToIntArr(bs);

                    length = br.ReadInt32();
                    bs = br.ReadBytes(length);
                    _dict = ByteArrToUshortArr(bs);


                    length = br.ReadInt32();
                    bs = br.ReadBytes(length);
                    _first = ByteArrToIntArr(bs);

                    length = br.ReadInt32();
                    bs = br.ReadBytes(length);
                    _end = ByteArrToIntArr(bs);

                    length = br.ReadInt32();
                    bs = br.ReadBytes(length);
                    _resultIndex = ByteArrToIntArr(bs);

                    var dictLength = br.ReadInt32();
                    _nextIndex = new IntDictionary[dictLength];


                    for (int i = 0; i < dictLength; i++)
                    {
                        length = br.ReadInt32();
                        bs = br.ReadBytes(length);
                        var keys = ByteArrToUshortArr(bs);

                        length = br.ReadInt32();
                        bs = br.ReadBytes(length);
                        var values = ByteArrToIntArr(bs);


                        IntDictionary dictionary = new IntDictionary(keys, values);
                        _nextIndex[i] = dictionary;
                    }
                }

                protected Int32[] ByteArrToIntArr(byte[] btArr)
                {
                    Int32 intSize = (int)Math.Ceiling(btArr.Length / (double)sizeof(Int32));
                    Int32[] intArr = new Int32[intSize];
                    Buffer.BlockCopy(btArr, 0, intArr, 0, btArr.Length);
                    return intArr;
                }
                protected ushort[] ByteArrToUshortArr(byte[] btArr)
                {
                    Int32 intSize = (int)Math.Ceiling(btArr.Length / (double)sizeof(ushort));
                    ushort[] intArr = new ushort[intSize];
                    Buffer.BlockCopy(btArr, 0, intArr, 0, btArr.Length);
                    return intArr;
                }
                #endregion
            }

            public struct IntDictionary
            {
                private ushort[] _keys;
                private int[] _values;
                private int last;
                public IntDictionary(ushort[] keys, int[] values)
                {
                    _keys = keys;
                    _values = values;
                    last = keys.Length - 1;
                }
                public IntDictionary(Dictionary<ushort, int> dict)
                {
                    var keys = dict.Select(q => q.Key).OrderBy(q => q).ToArray();
                    var values = new int[keys.Length];
                    for (int i = 0; i < keys.Length; i++)
                    {
                        values[i] = dict[keys[i]];
                    }
                    _keys = keys;
                    _values = values;
                    last = keys.Length - 1;
                }


                public ushort[] Keys
                {
                    get
                    {
                        return _keys;
                    }
                }

                public int[] Values
                {
                    get
                    {
                        return _values;
                    }
                }

                public bool TryGetValue(ushort key, out int value)
                {
                    if (last == -1)
                    {
                        value = 0;
                        return false;
                    }
                    if (_keys[0] == key)
                    {
                        value = _values[0];
                        return true;
                    }
                    else if (last == 0 || _keys[0] > key)
                    {
                        value = 0;
                        return false;
                    }

                    if (_keys[last] == key)
                    {
                        value = _values[last];
                        return true;
                    }
                    else if (_keys[last] < key)
                    {
                        value = 0;
                        return false;
                    }

                    var left = 1;
                    var right = last - 1;
                    while (left <= right)
                    {
                        int mid = (left + right) >> 1;
                        int d = _keys[mid] - key;

                        if (d == 0)
                        {
                            value = _values[mid];
                            return true;
                        }
                        else if (d > 0)
                        {
                            right = mid - 1;
                        }
                        else
                        {
                            left = mid + 1;
                        }
                    }
                    value = 0;
                    return false;
                }
            }

            public sealed class TrieNode //:IDisposable
            {
                public int Index;
                public int Layer;
                public bool End { get { return Results != null; } }
                public char Char;
                public List<int> Results;
                public Dictionary<char, TrieNode> m_values;
                public TrieNode Failure;
                public TrieNode Parent;
                public bool IsWildcard;
                public int WildcardLayer;
                public bool HasWildcard;


                public TrieNode Add(char c)
                {
                    TrieNode node;
                    if (m_values == null)
                    {
                        m_values = new Dictionary<char, TrieNode>();
                    }
                    else if (m_values.TryGetValue(c, out node))
                    {
                        return node;
                    }
                    node = new TrieNode();
                    node.Parent = this;
                    node.Char = c;
                    m_values[c] = node;
                    return node;
                }

                public void SetResults(int index)
                {

                    if (Results == null)
                    {
                        Results = new List<int>();
                    }
                    Results.Add(index);
                }
                /// <summary>
                /// 伪释放
                /// </summary>
                public void Dispose()
                {
                    if (Results != null)
                    {
                        Results.Clear();
                        Results = null;
                    }
                    if (m_values != null)
                    {
                        m_values.Clear();
                        m_values = null;
                    }
                    Failure = null;
                    Parent = null;
                }
            }
        }

        #endregion

        #region Combin

        public sealed class CombinMatcher
        {
            //private readonly ACMatcher _matcher;
            //private readonly IDictionary<string, List<(string, int)>> _map;

            //public CombinMatcher(IEnumerable<string> keywords)
            //{
            //    _map = new Dictionary<string, List<(string, int)>>();
            //    Trie trie = new Trie();
            //    foreach (var item in keywords)
            //    {
            //        CahceSplitKeyWordsDic(item);
            //        var words = item.Split('*', StringSplitOptions.RemoveEmptyEntries);
            //        foreach (var word in words)
            //        {
            //            trie.Insert(word);
            //        }
            //    }
            //    _matcher = new ACMatcher(new ACAutomaton(trie.Root));
            //}

            //private void CahceSplitKeyWordsDic(string item)
            //{
            //    var words = item.Split('*', StringSplitOptions.None);

            //    int i = 0;

            //    foreach (var word in words)
            //    {
            //        if (string.IsNullOrEmpty(word))
            //        {
            //            i++;
            //            continue;
            //        }

            //        this.InsertItem(i, item);

            //        i = 0;
            //    }

            //    if (i != 0)
            //    {
            //        this.InsertItem(i, item);
            //    }
            //}

            //private void InsertItem(int i, string item)
            //{
            //    if (!this._map.TryGetValue(item, out _))
            //    {
            //        _map.Add(item, new List<(string, int)>() { (string.Empty, i) });
            //    }
            //    else
            //    {
            //        _map[item].Add((string.Empty, i + 1));
            //    }
            //}

            //public IEnumerable<string> Searching(string source)
            //{
            //    var combinMatchs = this._matcher.FindCombinKeywords(source);
            //    if (!combinMatchs.Any())
            //    {
            //        return Array.Empty<string>();
            //    }

            //    var matchKeyWords = new List<string>();

            //    foreach (var (key, value) in this._map)
            //    {
            //        var splitKeys = value.Select(it => it.Item1).Where(it => !string.IsNullOrEmpty(it)).Distinct();

            //        if (!splitKeys.SequenceEqual(combinMatchs.Select(it => it.Item1).Where(it => splitKeys.Contains(it)).Distinct()))
            //        {
            //            continue;
            //        }

            //        int combileEndIndex = 0;

            //        foreach (var kv in value)
            //        {
            //            if (string.IsNullOrEmpty(kv.Item1) && ((source.Length - combileEndIndex <= (kv.Item2 * 10)) || kv.Item2 == 0))
            //            {
            //                matchKeyWords.Add(key);
            //            }

            //            var combin = combinMatchs.Where(it => it.Item1 == kv.Item1).FirstOrDefault();

            //            if (combin != default)
            //            {
            //                if (kv.Item2 == 0 || (kv.Item2 * 10) >= (combin.Item2.Item1 - combileEndIndex))
            //                {
            //                    combileEndIndex = combin.Item2.Item2;

            //                    if (value.Last().Equals(kv))
            //                    {
            //                        matchKeyWords.Add(key);
            //                    }
            //                }
            //                else
            //                {
            //                    break;
            //                }
            //            }

            //            combinMatchs.Remove(combin);
            //        }
            //    }

            //    return matchKeyWords;
            //}
        }

        #endregion

        #region Combin V2

        public sealed class CombinMatcherV2
        {
            private readonly string[] _patterns;

            public CombinMatcherV2(IEnumerable<string> keywords)
            {
                _patterns = this.GetPatterns(keywords).ToArray();
            }
            private IEnumerable<string> GetPatterns(IEnumerable<string> keywords)
            {

                foreach (var item in keywords.Where(k => k.Contains('*')))
                {
                    var items = item.Split('*');
                    var patternStr = new StringBuilder();
                    for (int i = 0; i < items.Length; i++)
                    {
                        var part = items[i];
                        if (string.IsNullOrEmpty(part))
                        {
                            patternStr.Append(".{0,10}");
                        }
                        else
                        {
                            patternStr.Append(part);
                            if (i < items.Length - 1)
                            {
                                patternStr.Append(".{0,10}");
                            }
                        }
                    }
                    yield return patternStr.ToString();
                }
            }

            public IEnumerable<string> Searching(string source)
            {
                var result = new ConcurrentBag<string>();
                Parallel.ForEach(this._patterns, (pattern, i) =>
                {
                    try
                    {
                        foreach (Match item in Regex.Matches(source, pattern))
                        {
                            result.Add(item.Value);
                        }
                    }
                    catch
                    {
                    }
                });

                return result;
            }
        }

        #endregion

        #region Combin V3

        class CombinMatcherV3
        {
            private readonly WordsMatch _wordsSearch;
            public CombinMatcherV3(IEnumerable<string> keywords)
            {
                _wordsSearch = new WordsMatch();
                foreach (var word in keywords)
                {
                    var items = word.AsSpan();
                    var list = new List<string>();
                    for (int i = 0; i < items.Length; i++)
                    {
                        Insert(items[i], list);
                    }
                    list.Add(word.Replace("*", string.Empty));
                    try
                    {
                        _wordsSearch.SetKeywords(list.Distinct().ToArray());
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            void Insert(char currentChar, List<string> list)
            {
                if (currentChar == '*')
                {
                    if (list.Any())
                    {
                        var count = list.Count;
                        for (int i = 0; i < count; i++)
                        {
                            for (int j = 1; j <= 10; j++)
                            {
                                list.Add($"{list[i]}{".".PadLeft(j, '.')}");
                            }
                        }
                        list.RemoveRange(0, count);
                    }
                    else
                    {
                        for (int i = 1; i <= 10; i++)
                        {
                            list.Add(".".PadLeft(i, '.'));
                        }
                    }
                }
                else
                {
                    if (list.Any())
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            list[i] = $"{list[i]}{currentChar}";
                        }
                    }
                    else
                    {
                        list.Add($"{currentChar}");
                    }
                }
            }

            public IEnumerable<string> Searching(string source)
            {
                return this._wordsSearch.FindAll(source).Select(r => r.Keyword);
            }

            public class WordsMatch : BaseMatch
            {
                /// <summary>
                /// 在文本中查找所有的关键字
                /// </summary>
                /// <param name="text">文本</param>
                /// <returns></returns>
                public List<WordsSearchResult> FindAll(string text)
                {
                    TrieNode3 ptr = null;
                    List<WordsSearchResult> list = new List<WordsSearchResult>();

                    for (int i = 0; i < text.Length; i++)
                    {
                        var t = text[i];
                        TrieNode3 tn;
                        if (ptr == null)
                        {
                            tn = _first[t];
                        }
                        else
                        {
                            if (ptr.TryGetValue(t, out tn) == false)
                            {
                                if (ptr.HasWildcard)
                                {
                                    FindAll(text, i + 1, ptr.WildcardNode, list);
                                }
                                tn = _first[t];
                            }
                        }
                        if (tn != null)
                        {
                            if (tn.End)
                            {
                                foreach (var r in tn.Results)
                                {
                                    var length = _keywordLength[r];
                                    var start = i - length + 1;
                                    if (start >= 0)
                                    {
                                        var kIndex = _keywordIndex[r];
                                        var matchKeyword = _matchKeywords[kIndex];
                                        var keyword = text.Substring(start, length);
                                        var result = new WordsSearchResult(keyword, start, i, kIndex, matchKeyword);
                                        list.Add(result);
                                    }
                                }
                            }
                        }
                        ptr = tn;
                    }
                    return list;
                }
                private void FindAll(string text, int index, TrieNode3 ptr, List<WordsSearchResult> list)
                {
                    for (int i = index; i < text.Length; i++)
                    {
                        var t = text[i];
                        TrieNode3 tn;
                        if (ptr.TryGetValue(t, out tn) == false)
                        {
                            if (ptr.HasWildcard)
                            {
                                FindAll(text, i + 1, ptr.WildcardNode, list);
                            }
                            return;
                        }
                        if (tn.End)
                        {
                            foreach (var r in tn.Results)
                            {
                                var length = _keywordLength[r];
                                var start = i - length + 1;
                                if (start >= 0)
                                {
                                    var kIndex = _keywordIndex[r];
                                    var matchKeyword = _matchKeywords[kIndex];
                                    var keyword = text.Substring(start, length);
                                    var result = new WordsSearchResult(keyword, start, i, kIndex, matchKeyword);
                                    list.Add(result);
                                }
                            }
                        }
                        ptr = tn;
                    }
                }
            }

            public abstract class BaseMatch
            {
                protected TrieNode3[] _first;
                protected internal int[] _keywordLength;
                protected internal int[] _keywordIndex;
                protected internal string[] _matchKeywords;

                #region BuildFirstLayerTrieNode
                protected List<TrieNode> BuildFirstLayerTrieNode(List<string> keywords)
                {
                    var root = new TrieNode();

                    Dictionary<int, List<TrieNode>> allNodeLayers = new Dictionary<int, List<TrieNode>>();
                    #region 第一次关键字
                    for (int i = 0; i < keywords.Count; i++)
                    {
                        var p = keywords[i];
                        var nd = root;
                        var start = 0;
                        while (p[start] == 0)
                        { // 0 为 通配符
                            start++;
                        }
                        for (int j = start; j < p.Length; j++)
                        {
                            nd = nd.Add((char)p[j]);
                            if (nd.Layer == 0)
                            {
                                nd.Layer = j + 1 - start;
                                List<TrieNode> trieNodes;
                                if (allNodeLayers.TryGetValue(nd.Layer, out trieNodes) == false)
                                {
                                    trieNodes = new List<TrieNode>();
                                    allNodeLayers[nd.Layer] = trieNodes;
                                }
                                trieNodes.Add(nd);
                            }
                        }
                        nd.SetResults(i);
                    }
                    #endregion

                    #region 第二次关键字 通配符
                    for (int i = 0; i < keywords.Count; i++)
                    {
                        var p = keywords[i];
                        if (p.Contains((char)0) == false)
                        {
                            continue;
                        }
                        var start = 0;
                        while (p[start] == 0)
                        { // 0 为 通配符
                            start++;
                        }
                        List<TrieNode> trieNodes = new List<TrieNode>() { root };

                        for (int j = start; j < p.Length; j++)
                        {
                            List<TrieNode> newTrieNodes = new List<TrieNode>();
                            var c = p[j];
                            if (c == 0)
                            {
                                foreach (var nd in trieNodes.Where(n => n.m_values?.Any() ?? false))
                                {
                                    newTrieNodes.AddRange(nd.m_values.Values);
                                }
                            }
                            else
                            {
                                foreach (var nd in trieNodes)
                                {
                                    var nd2 = nd.Add(c);
                                    if (nd2.Layer == 0)
                                    {
                                        nd2.Layer = j + 1 - start;
                                        List<TrieNode> tnodes;
                                        if (allNodeLayers.TryGetValue(nd2.Layer, out tnodes) == false)
                                        {
                                            tnodes = new List<TrieNode>();
                                            allNodeLayers[nd.Layer] = tnodes;
                                        }
                                        tnodes.Add(nd2);
                                    }
                                    newTrieNodes.Add(nd2);
                                }
                            }
                            trieNodes = newTrieNodes;
                        }
                        foreach (var nd in trieNodes)
                        {
                            nd.SetResults(i);
                        }
                    }
                    #endregion

                    #region 添加到 allNode
                    var allNode = new List<TrieNode>();
                    allNode.Add(root);
                    foreach (var trieNodes in allNodeLayers)
                    {
                        foreach (var nd in trieNodes.Value)
                        {
                            allNode.Add(nd);
                        }
                    }
                    allNodeLayers.Clear();
                    allNodeLayers = null;
                    #endregion

                    #region 第一次 Set Failure
                    for (int i = 1; i < allNode.Count; i++)
                    {
                        var nd = allNode[i];
                        nd.Index = i;
                        TrieNode r = nd.Parent.Failure;
                        char c = nd.Char;
                        while (r != null && (r.m_values == null || !r.m_values.ContainsKey(c))) r = r.Failure;
                        if (r == null)
                            nd.Failure = root;
                        else
                        {
                            nd.Failure = r.m_values[c];
                            if (nd.Failure.Results != null)
                            {
                                foreach (var result in nd.Failure.Results)
                                    nd.SetResults(result);
                            }
                        }
                    }
                    #endregion

                    #region 第二次 Set Failure
                    for (int i = 1; i < allNode.Count; i++)
                    {
                        var nd = allNode[i];
                        if (nd.Layer == 1) { continue; }
                        if (nd.m_values != null)
                        {
                            if (nd.m_values.ContainsKey((char)0))
                            {
                                nd.HasWildcard = true;
                            }
                        }
                        if (nd.Failure.HasWildcard)
                        {
                            nd.HasWildcard = true;
                        }
                        if (nd.Char == 0)
                        {
                            nd.IsWildcard = true;
                            continue;
                        }
                        else if (nd.Parent.IsWildcard)
                        {
                            nd.IsWildcard = true;
                            nd.WildcardLayer = nd.Parent.WildcardLayer + 1;
                            if (nd.Failure != root)
                            {
                                if (nd.Failure.Layer <= nd.WildcardLayer)
                                {
                                    nd.Failure = root;
                                }
                            }
                            continue;
                        }
                    }
                    root.Failure = root;
                    #endregion

                    return allNode;
                }
                #endregion

                #region HasMatch
                protected bool HasMatch(string keyword)
                {
                    for (int i = 0; i < keyword.Length; i++)
                    {
                        char c = keyword[i];
                        if (c == '.' || c == '?' || c == '\\' || c == '[' || c == '(')
                        {
                            return true;
                        }
                    }
                    return false;
                }
                #endregion

                #region MatchKeywordBuild
                protected List<string> MatchKeywordBuild(string keyword)
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    Dictionary<int, List<string>> parameterDict = new Dictionary<int, List<string>>();
                    SeparateParameters(keyword, stringBuilder, parameterDict);

                    if (parameterDict.Count == 0)
                    {
                        return new List<string>() { stringBuilder.ToString() };
                    }
                    List<string> parameters = new List<string>();
                    KeywordBuild(parameterDict, 0, parameterDict.Keys.Count - 1, "", parameters);
                    var keywordFmt = stringBuilder.ToString();
                    HashSet<string> list = new HashSet<string>();
                    foreach (var item in parameters)
                    {
                        list.Add(string.Format(keywordFmt, item.Split((char)1)));
                    }
                    return list.ToList();
                }

                private static void SeparateParameters(string keyword, StringBuilder stringBuilder, Dictionary<int, List<string>> parameterDict)
                {
                    var index = 0;
                    var parameterIndex = 0;

                    while (index < keyword.Length)
                    {
                        var c = keyword[index];
                        if (c == '.')
                        {
                            if (index + 1 < keyword.Length && keyword[index + 1] == '?')
                            {
                                parameterDict[parameterIndex] = new List<string>() { "", ((char)0).ToString() };
                                stringBuilder.Append("{" + parameterIndex + "}");
                                parameterIndex++;
                                index += 2;

                            }
                            else
                            {
                                stringBuilder.Append(((char)0));
                                index++;
                            }
                        }
                        else if (c == '\\')
                        {
                            if (index + 2 < keyword.Length && keyword[index + 2] == '?')
                            {
                                parameterDict[parameterIndex] = new List<string>() { "", keyword[index + 1].ToString() };
                                stringBuilder.Append("{" + parameterIndex + "}");
                                parameterIndex++;
                                index += 3;
                            }
                            else if (index + 1 < keyword.Length)
                            {
                                stringBuilder.Append(keyword[index + 1]);
                                index += 2;
                            }
                            else
                            {
                                throw new Exception($"【{keyword}】出错了，最后一位为\\");
                            }
                        }
                        else if (c == '[')
                        {
                            index++;
                            var ps = new List<string>();
                            while (index < keyword.Length)
                            {
                                c = keyword[index];
                                if (c == ']')
                                {
                                    break;
                                }
                                else if (c == '\\')
                                {
                                    if (index + 1 < keyword.Length)
                                    {
                                        ps.Add(keyword[index + 1].ToString());
                                        index += 2;
                                    }
                                }
                                else
                                {
                                    ps.Add(c.ToString());
                                    index++;
                                }
                            }
                            if (c != ']')
                            {
                                throw new Exception($"【{keyword}】出错了，最后一位不为]");
                            }
                            if (index + 1 < keyword.Length && keyword[index + 1] == '?')
                            {
                                ps.Insert(0, "");
                                parameterDict[parameterIndex] = ps;
                                stringBuilder.Append("{" + parameterIndex + "}");
                                parameterIndex++;
                                index += 2;
                            }
                            else
                            {
                                parameterDict[parameterIndex] = ps;
                                stringBuilder.Append("{" + parameterIndex + "}");
                                parameterIndex++;
                                index++;
                            }
                        }
                        else if (c == '(')
                        {
                            index++;
                            var ps = new List<string>();
                            var words = "";
                            while (index < keyword.Length)
                            {
                                c = keyword[index];
                                if (c == ')')
                                {
                                    break;
                                }
                                else if (c == '|')
                                {
                                    ps.Add(words);
                                    words = "";
                                    index++;
                                }
                                else if (c == '\\')
                                {
                                    if (index + 1 < keyword.Length)
                                    {
                                        words += keyword[index + 1];
                                        index += 2;
                                    }
                                }
                                else
                                {
                                    words += c;
                                    index++;
                                }
                            }
                            ps.Add(words);
                            if (c != ')')
                            {
                                throw new Exception($"【{keyword}】出错了，最后一位不为)");
                            }
                            if (index + 1 < keyword.Length && keyword[index + 1] == '?')
                            {
                                ps.Insert(0, "");
                                parameterDict[parameterIndex] = ps;
                                stringBuilder.Append("{" + parameterIndex + "}");
                                parameterIndex++;
                                index += 2;
                            }
                            else
                            {
                                parameterDict[parameterIndex] = ps;
                                stringBuilder.Append("{" + parameterIndex + "}");
                                parameterIndex++;
                                index++;
                            }
                        }
                        else
                        {
                            if (index + 1 < keyword.Length && keyword[index + 1] == '?')
                            {
                                parameterDict[parameterIndex] = new List<string>() { "", c.ToString() };
                                stringBuilder.Append("{" + parameterIndex + "}");
                                parameterIndex++;
                                index += 2;
                            }
                            else
                            {
                                if (c == '{')
                                {
                                    stringBuilder.Append("{{");
                                }
                                else if (c == '}')
                                {
                                    stringBuilder.Append("}}");
                                }
                                else
                                {
                                    stringBuilder.Append(c);
                                }
                                index++;
                            }
                        }
                    }
                }

                private static void KeywordBuild(Dictionary<int, List<string>> parameterDict, int index, int end, string keyword, List<string> result)
                {
                    const char span = (char)1;
                    var list = parameterDict[index];
                    if (index == end)
                    {
                        foreach (var item in list)
                        {
                            result.Add((keyword + span + item).Substring(1));
                        }
                    }
                    else
                    {
                        foreach (var item in list)
                        {
                            KeywordBuild(parameterDict, index + 1, end, keyword + span + item, result);
                        }
                    }
                }

                #endregion

                #region SetKeywords
                /// <summary>
                /// 设置关键字
                /// </summary>
                /// <param name="keywords">关键字列表</param>
                public virtual void SetKeywords(ICollection<string> keywords)
                {
                    _matchKeywords = keywords.ToArray();
                    List<string> newKeyword = new List<string>();
                    List<int> newKeywordLength = new List<int>();
                    List<int> newKeywordIndex = new List<int>();
                    var index = 0;
                    foreach (var keyword in keywords)
                    {
                        if (HasMatch(keyword) == false)
                        {
                            newKeyword.Add(keyword);
                            newKeywordLength.Add(keyword.Length);
                            newKeywordIndex.Add(index);
                        }
                        else
                        {
                            var list = MatchKeywordBuild(keyword);
                            foreach (var item in list)
                            {
                                newKeyword.Add(item);
                                newKeywordLength.Add(item.Length);
                                newKeywordIndex.Add(index);
                            }
                        }
                        index++;
                    }
                    _keywordLength = newKeywordLength.ToArray();
                    _keywordIndex = newKeywordIndex.ToArray();

                    SetKeywords2(newKeyword);
                }

                #endregion

                #region SetKeywords2

                protected virtual void SetKeywords2(List<string> keywords)
                {
                    List<TrieNode> allNode = BuildFirstLayerTrieNode(keywords);
                    TrieNode root = allNode[0];

                    var allNode2 = new List<TrieNode3>();
                    for (int i = 0; i < allNode.Count; i++)
                    {
                        allNode2.Add(new TrieNode3());
                    }
                    for (int i = allNode2.Count - 1; i >= 0; i--)
                    {
                        var oldNode = allNode[i];
                        var newNode = allNode2[i];

                        if (oldNode.m_values != null)
                        {
                            foreach (var item in oldNode.m_values)
                            {
                                var key = item.Key;
                                var index = item.Value.Index;
                                if (key == 0)
                                {
                                    newNode.HasWildcard = true;
                                    newNode.WildcardNode = allNode2[index];
                                    continue;
                                }
                                newNode.Add(key, allNode2[index]);
                            }
                        }
                        if (oldNode.Results != null)
                        {
                            foreach (var item in oldNode.Results)
                            {
                                if (oldNode.IsWildcard)
                                {
                                    if (keywords[item].Length > oldNode.WildcardLayer)
                                    {
                                        newNode.SetResults(item);
                                    }
                                }
                                else
                                {
                                    newNode.SetResults(item);
                                }
                                //newNode.SetResults(item);
                            }
                        }


                        var failure = oldNode.Failure;
                        while (failure != root)
                        {
                            if (oldNode.IsWildcard && failure.Layer <= oldNode.WildcardLayer)
                            {
                                break;
                            }
                            if (failure.m_values != null)
                            {
                                foreach (var item in failure.m_values)
                                {
                                    var key = item.Key;
                                    var index = item.Value.Index;
                                    if (key == 0)
                                    {
                                        newNode.HasWildcard = true;
                                        if (newNode.WildcardNode == null)
                                        {
                                            newNode.WildcardNode = allNode2[index];
                                        }
                                        continue;
                                    }
                                    if (newNode.HasKey(key) == false)
                                    {
                                        newNode.Add(key, allNode2[index]);
                                    }
                                }
                            }
                            if (failure.Results != null)
                            {
                                foreach (var item in failure.Results)
                                {
                                    if (oldNode.IsWildcard)
                                    {
                                        if (keywords[item].Length > oldNode.WildcardLayer)
                                        {
                                            newNode.SetResults(item);
                                        }
                                    }
                                    else
                                    {
                                        newNode.SetResults(item);
                                    }
                                }
                            }
                            failure = failure.Failure;
                        }
                        allNode[i].Dispose();
                    }
                    allNode.Clear();
                    allNode = null;
                    root = null;

                    //var root2 = allNode2[0];
                    TrieNode3[] first = new TrieNode3[char.MaxValue + 1];
                    foreach (var item in allNode2[0].m_values)
                    {
                        first[item.Key] = item.Value;
                    }
                    _first = first;
                }
                #endregion
            }

            public sealed class TrieNode3
            {
                public bool End { get { return Results != null; } }
                public bool HasWildcard;
                public List<int> Results;
                public Dictionary<char, TrieNode3> m_values;
                private uint minflag = uint.MaxValue;
                private uint maxflag = uint.MinValue;
                public TrieNode3 WildcardNode;


                public void Add(char c, TrieNode3 node3)
                {
                    if (minflag > c) { minflag = c; }
                    if (maxflag < c) { maxflag = c; }
                    if (m_values == null)
                    {
                        m_values = new Dictionary<char, TrieNode3>();
                    }
                    m_values.Add(c, node3);
                }

                public void SetResults(int index)
                {
                    if (Results == null)
                    {
                        Results = new List<int>();
                    }
                    if (Results.Contains(index) == false)
                    {
                        Results.Add(index);
                    }
                }

                public bool HasKey(char c)
                {
                    if (m_values == null)
                    {
                        return false;
                    }
                    return m_values.ContainsKey(c);
                }

                public bool TryGetValue(char c, out TrieNode3 node)
                {
                    if (minflag <= (uint)c && maxflag >= (uint)c)
                    {
                        return m_values.TryGetValue(c, out node);
                    }
                    node = null;
                    return false;
                }
            }
            public struct IntDictionary
            {
                private ushort[] _keys;
                private int[] _values;
                private int last;
                public IntDictionary(ushort[] keys, int[] values)
                {
                    _keys = keys;
                    _values = values;
                    last = keys.Length - 1;
                }
                public IntDictionary(Dictionary<ushort, int> dict)
                {
                    var keys = dict.Select(q => q.Key).OrderBy(q => q).ToArray();
                    var values = new int[keys.Length];
                    for (int i = 0; i < keys.Length; i++)
                    {
                        values[i] = dict[keys[i]];
                    }
                    _keys = keys;
                    _values = values;
                    last = keys.Length - 1;
                }


                public ushort[] Keys
                {
                    get
                    {
                        return _keys;
                    }
                }

                public int[] Values
                {
                    get
                    {
                        return _values;
                    }
                }

                public bool TryGetValue(ushort key, out int value)
                {
                    if (last == -1)
                    {
                        value = 0;
                        return false;
                    }
                    if (_keys[0] == key)
                    {
                        value = _values[0];
                        return true;
                    }
                    else if (last == 0 || _keys[0] > key)
                    {
                        value = 0;
                        return false;
                    }

                    if (_keys[last] == key)
                    {
                        value = _values[last];
                        return true;
                    }
                    else if (_keys[last] < key)
                    {
                        value = 0;
                        return false;
                    }

                    var left = 1;
                    var right = last - 1;
                    while (left <= right)
                    {
                        int mid = (left + right) >> 1;
                        int d = _keys[mid] - key;

                        if (d == 0)
                        {
                            value = _values[mid];
                            return true;
                        }
                        else if (d > 0)
                        {
                            right = mid - 1;
                        }
                        else
                        {
                            left = mid + 1;
                        }
                    }
                    value = 0;
                    return false;
                }
            }
            public sealed class TrieNode //:IDisposable
            {
                public int Index;
                public int Layer;
                public bool End { get { return Results != null; } }
                public char Char;
                public List<int> Results;
                public Dictionary<char, TrieNode> m_values;
                public TrieNode Failure;
                public TrieNode Parent;
                public bool IsWildcard;
                public int WildcardLayer;
                public bool HasWildcard;


                public TrieNode Add(char c)
                {
                    TrieNode node;
                    if (m_values == null)
                    {
                        m_values = new Dictionary<char, TrieNode>();
                    }
                    else if (m_values.TryGetValue(c, out node))
                    {
                        return node;
                    }
                    node = new TrieNode();
                    node.Parent = this;
                    node.Char = c;
                    m_values[c] = node;
                    return node;
                }

                public void SetResults(int index)
                {

                    if (Results == null)
                    {
                        Results = new List<int>();
                    }
                    Results.Add(index);
                }
                /// <summary>
                /// 伪释放
                /// </summary>
                public void Dispose()
                {
                    if (Results != null)
                    {
                        Results.Clear();
                        Results = null;
                    }
                    if (m_values != null)
                    {
                        m_values.Clear();
                        m_values = null;
                    }
                    Failure = null;
                    Parent = null;
                }
            }
            public sealed class TrieNode3Ex
            {
                public int Index;
                public bool End { get { return Results != null; } }
                public List<int> Results;
                public Dictionary<char, TrieNode3Ex> m_values;
                public ushort minflag = ushort.MaxValue;
                public ushort maxflag = ushort.MinValue;
                public bool HasWildcard;
                public TrieNode3Ex WildcardNode;

                public void Add(char c, TrieNode3Ex node3)
                {
                    if (minflag > c) { minflag = c; }
                    if (maxflag < c) { maxflag = c; }
                    if (m_values == null)
                    {
                        m_values = new Dictionary<char, TrieNode3Ex>();
                    }
                    m_values.Add(c, node3);
                }

                public void SetResults(int index)
                {
                    if (Results == null)
                    {
                        Results = new List<int>();
                    }
                    if (Results.Contains(index) == false)
                    {
                        Results.Add(index);
                    }
                }

                public bool HasKey(char c)
                {
                    if (m_values == null)
                    {
                        return false;
                    }
                    return m_values.ContainsKey(c);
                }
            }

            public class WordsSearchResult
            {
                public WordsSearchResult(string keyword, int start, int end, int index)
                {
                    _keyword = keyword;
                    End = end;
                    Start = start;
                    Index = index;
                    _matchKeyword = keyword;
                }

                public WordsSearchResult(ref string text, int start, int end, int index)
                {
                    _text = text;
                    End = end;
                    Start = start;
                    Index = index;
                }


                public WordsSearchResult(string keyword, int start, int end, int index, string matchKeyword)
                {
                    _keyword = keyword;
                    End = end;
                    Start = start;
                    Index = index;
                    _matchKeyword = matchKeyword;
                }
                private string _text;
                private string _keyword;
                private string _matchKeyword;

                /// <summary>
                /// 开始位置
                /// </summary>
                public int Start { get; private set; }

                /// <summary>
                /// 结束位置
                /// </summary>
                public int End { get; private set; }


                /// <summary>
                /// 关键字
                /// </summary>
                public string Keyword
                {
                    get
                    {
                        if (_keyword == null)
                        {
                            _keyword = _text.Substring(Start, End + 1 - Start);
                        }
                        return _keyword;
                    }
                }

                /// <summary>
                /// 索引
                /// </summary>
                public int Index { get; private set; }

                /// <summary>
                /// 匹配关键字
                /// </summary>
                public string MatchKeyword
                {
                    get
                    {
                        if (_matchKeyword == null)
                        {
                            if (_keyword == null)
                            {
                                _matchKeyword = _text.Substring(Start, End + 1 - Start);
                            }
                            else
                            {
                                _matchKeyword = _keyword;
                            }
                        }
                        return _matchKeyword;
                    }
                }


                public override string ToString()
                {
                    if (MatchKeyword != Keyword)
                    {
                        return Start.ToString() + "|" + Keyword + "|" + MatchKeyword;
                    }
                    return Start.ToString() + "|" + Keyword;
                }
            }
        }

        #endregion

    }
}

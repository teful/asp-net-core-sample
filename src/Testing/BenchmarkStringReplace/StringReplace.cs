﻿using BenchmarkDotNet.Attributes;
using System.Text;

namespace BenchmarkStringReplace
{
    [MemoryDiagnoser]
    [ShortRunJob]
    public class StringReplace
    {
        readonly Dictionary<string, string> map = new()
        {
            { "×", "*" },
            { "÷", "/" },
            { "SIN", "Sin" },
            { "COS", "Cos" },
            { "TAN", "Tan" },
            { "ASIN", "Asin" },
            { "ACOS", "Acos" },
            { "ATAN", "Atan" },
            { "LOG", "Log" },
            { "EXP", "Exp" },
            { "LOG10", "Log10" },
            { "POW", "Pow" },
            { "SQRT", "Sqrt" },
            { "ABS", "Abs" },
        };

        private const string target = "1 x 1 ÷ 1 * SIN(COS(TAN(LOG(ASIN(ATAN(EXP(POW(SQRT(ABS(1))))))))))  |" +
                                      "1 x 1 ÷ 1 * SIN(COS(TAN(LOG(ASIN(ATAN(EXP(POW(SQRT(ABS(1))))))))))  |" +
                                      "1 x 1 ÷ 1 * SIN(COS(TAN(LOG(ASIN(ATAN(EXP(POW(SQRT(ABS(1))))))))))  |" +
                                      "1 x 1 ÷ 1 * SIN(COS(TAN(LOG(ASIN(ATAN(EXP(POW(SQRT(ABS(1))))))))))  |" +
                                      "1 x 1 ÷ 1 * SIN(COS(TAN(LOG(ASIN(ATAN(EXP(POW(SQRT(ABS(1))))))))))  |" +
                                      "1 x 1 ÷ 1 * SIN(COS(TAN(LOG(ASIN(ATAN(EXP(POW(SQRT(ABS(1))))))))))  |" +
                                      "1 x 1 ÷ 1 * SIN(COS(TAN(LOG(ASIN(ATAN(EXP(POW(SQRT(ABS(1))))))))))  |" +
                                      "1 x 1 ÷ 1 * SIN(COS(TAN(LOG(ASIN(ATAN(EXP(POW(SQRT(ABS(1))))))))))  |" +
                                      "1 x 1 ÷ 1 * SIN(COS(TAN(LOG(ASIN(ATAN(EXP(POW(SQRT(ABS(1))))))))))  |";

        [Benchmark]
        public string String()
        {
            var result = target;
            foreach (var key in map.Keys)
            {
                result = result.Replace(key, map[key]);
            }

            return result;
        }

        [Benchmark]
        public string StringBuilder()
        {
            var result = new StringBuilder(target);
            foreach (var key in map.Keys)
            {
                result.Replace(key, map[key]);
            }

            return result.ToString();
        }
    }
}

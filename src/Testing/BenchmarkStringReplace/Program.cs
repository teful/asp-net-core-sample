﻿using BenchmarkDotNet.Running;

namespace BenchmarkStringReplace
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            BenchmarkRunner.Run<StringReplace>();
        }
    }
}
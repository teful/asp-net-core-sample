﻿using System.Text.RegularExpressions;

namespace PrettyNumber
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            do
            {
                var mobile = Console.ReadLine();
                if (string.IsNullOrEmpty(mobile))
                {
                    continue;
                }
                if (mobile == "q")
                {
                    break;
                }

                Matching(mobile);

            } while (true);

            Console.ReadKey();

        }

        static void Matching(string mobile)
        {
            Console.WriteLine("********************");
            var flag = false;
            foreach (var rule in CreateRules().OrderByDescending(r => r.Length))
            {
                var match = Regex.Match(mobile, rule.Pattern);

                if (!match.Success)
                {
                    continue;
                }

                for (int i = 1; i < match.Groups.Count; i++)
                {
                    Group item = match.Groups[i];

                    var endLength = item.Index + rule.Length;
                    if (endLength > mobile.Length)
                    {
                        continue;
                    }
                    Console.WriteLine($"{rule.Name} : {mobile}({mobile[item.Index..endLength]})");
                    flag = true;
                }

                if (flag)
                {
                    break;
                }
            }
            Console.WriteLine("////////////////////");
        }


        static IEnumerable<RuleInfo> CreateRules()
        {
            return new List<RuleInfo>
            {
                new RuleInfo("包含8A",".+(\\d)\\1{7,}",8),
                new RuleInfo("包含AABBCCDD","\\d([0-9])\\1([0-9])\\2([0-9])\\3([0-9])\\4",8),
                new RuleInfo("尾AAABAAAB",".+(\\d)\\1\\1(\\d)\\1\\1\\1\\2$",8),
                new RuleInfo("包含7A",".+(\\d)\\1{6,}",7),
                new RuleInfo("包含6A",".+(\\d)\\1{5,}",6),
                new RuleInfo("包含5A",".+(\\d)\\1{4,}",5),
                new RuleInfo("包含AAAA",".+(\\d)\\1{3,}",4),
                new RuleInfo("包含AAA",".+(\\d)\\1{2,}",3),
                new RuleInfo("包含AAABBB",".+(\\d)\\1{2,5}?(\\d)\\2{2,5}?",6),
                new RuleInfo("包含ABABAB",".+(\\d)((?!\\1)\\d)\\1\\2\\1\\2",6),
                new RuleInfo("尾AAA",".+(\\d)\\1{2,}$",3),
                new RuleInfo("尾AAAB",".+(\\d)\\1{2,5}?(\\d)$",4),
                new RuleInfo("尾AAABA",".+(\\d)\\1\\1(\\d)\\1$",5),
                new RuleInfo("尾AAABBB",".+(\\d)\\1{2,5}?(\\d)\\2{2,5}?$",6),
                new RuleInfo("尾AABAA",".+(\\d)\\1(\\d)\\1\\1$",5),
                new RuleInfo("尾AABB","\\d{6}([0-9])(?!1)([0-9])\\2(?!2)([0-9])\\3",4),
                new RuleInfo("尾AABBB","\\d{6}([0-9])\\1{1}(?!1)([0-9])\\2{2}",5),
                new RuleInfo("尾AABBBB","\\d{5}([0-9])\\1{1}(?!1)([0-9])\\2{3}",6),
                new RuleInfo("尾AABBBBB","\\d{4}([0-9])\\1{1}(?!1)([0-9])\\2{4}",7),
                //new RuleInfo("尾AABBCC","\\d{4}([0-9])(?!1)([0-9])\\2(?!2)([0-9])\\3(?!3)([0-9])\\4",6),//15135555588 误判：555558
                //new RuleInfo("尾AABBCCC","\\d{3}([0-9])(?!1)([0-9])\\2{1}(?!2)([0-9])\\3{1}(?!3)([0-9])\\4{2}",7),//15135555888 误判：3555588
                new RuleInfo("尾AABBCCCC","\\d{2}([0-9])(?!1)([0-9])\\2{1}(?!2)([0-9])\\3{1}(?!3)([0-9])\\4{3}",8),
                new RuleInfo("尾AABBCCDD","\\d{3}([0-9])\\1([0-9])\\2([0-9])\\3([0-9])\\4",8),
                new RuleInfo("尾ABABABAB","\\d{3}(\\d)((?!\\1)\\d)\\1\\2\\1\\2\\1\\2",8),
                //new RuleInfo("尾ABABCCC",".+(\\d)(?!1)(\\d)\\1\\2(\\d)\\3{2}$",7),//15135555888(5555888)
                new RuleInfo("尾ABABCCCC",".+(\\d)(?!1)(\\d)\\1\\2(\\d)\\3{3}$",8),
                new RuleInfo("尾ABC",".+(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)){2}\\d$",3),
                new RuleInfo("尾ABCD",".+(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)){3}\\d$",4),
                new RuleInfo("尾ABCABC","\\d{5}(\\d{3})\\1$",6),
                new RuleInfo("尾ABCDABCD","\\d{3}(\\d{4})\\1$",8),
                new RuleInfo("尾ABCDE",".+(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)){4}\\d$",5),
                new RuleInfo("尾ABCDEF",".+(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)){5}\\d$",6),
                new RuleInfo("尾1349任意组合","^\\d{7}(1|3|4|9)(1|3|4|9)(1|3|4|9)(1|3|4|9)",4),
                new RuleInfo("尾2678任意组合","^\\d{7}(2|6|7|8)(2|6|7|8)(2|6|7|8)(2|6|7|8)",4),
                new RuleInfo("尾正3顺",".+(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)){2}\\d$",3),
                new RuleInfo("尾倒3顺",".+(?:9(?=8)|8(?=7)|7(?=6)|6(?=5)|5(?=4)|4(?=3)|3(?=2)|2(?=1)|1(?=0)){2}\\d$",3),
                new RuleInfo("尾正4顺",".+(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)){3}\\d$",4),
                new RuleInfo("尾倒4顺",".+(?:9(?=8)|8(?=7)|7(?=6)|6(?=5)|5(?=4)|4(?=3)|3(?=2)|2(?=1)|1(?=0)){3}\\d$",4),
                new RuleInfo("尾520","^\\d{8}(520)",3),
                new RuleInfo("尾号8且最后4位不包含4","^\\d{7}(0|1|2|3|5|6|7|8|9){3}(8)",4),
                new RuleInfo("生日号1960-2022","\\d{3}(19[6-9]\\d|20[0-2][0-2])",4),
                new RuleInfo("尾ABAB","^\\d{7}(\\d)(?!\\1)(\\d)\\1\\2",4),
                new RuleInfo("中间正4顺","\\d{3}(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)){3}\\d\\d{4}",4),
                new RuleInfo("中间倒4顺","^\\d{3}(?:9(?=8)|8(?=7)|7(?=6)|6(?=5)|5(?=4)|4(?=3)|3(?=2)|2(?=1)|1(?=0)){3}\\d\\d{4}",4),
            };
        }
    }
}
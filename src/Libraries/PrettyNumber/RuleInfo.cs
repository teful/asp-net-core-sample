﻿namespace PrettyNumber
{
    internal class RuleInfo
    {
        public RuleInfo(string name, string regex, int length)
        {
            Name = name;
            Pattern = regex;
            Length = length;
        }

        public string Name { get; set; }
        public string Pattern { get; set; }
        public int Length { get; set; }
    }
}

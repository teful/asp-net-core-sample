# DotNet Sample

#### 介绍

**万物皆服务，配置前移的 DI 风格**

在平时工作中遇到的或已解决的问题示例

#### 结构
- [基于asp.net core identity授权扩展示例](https://gitee.com/teful/asp-net-core-sample/tree/master/src/Authorization/AuthorizationApiSample)
- [.net core 中服务和中间件的配置源码示例](https://gitee.com/teful/asp-net-core-sample/tree/master/src/CodeSegments/DotnetInternalConfigureOptionsSample)
- [使用DotNetty示例](https://gitee.com/teful/asp-net-core-sample/tree/master/src/DotNettyExamples)
- [模仿.net core中间件实现示例](https://gitee.com/teful/asp-net-core-sample/tree/master/src/Middleware/Example)
- [性能测试](https://gitee.com/teful/asp-net-core-sample/tree/master/src/Testing/BenchmarkDotNetSample)
- [系统后台服务](https://gitee.com/teful/asp-net-core-sample/tree/master/src/BackgroundServices/WorkerServiceExamples)
- [数据库代替配置文件](https://gitee.com/teful/asp-net-core-sample/tree/master/src/CodeSegments/CustomeDatabaseConfiguration)
- [ChangeToken 方式回调示例](https://gitee.com/teful/asp-net-core-sample/tree/master/src/CodeSegments/CallbackOfChangeTokenModeSample)

